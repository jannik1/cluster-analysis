import random

import pandas
import json

from nltk import word_tokenize
from sklearn.decomposition import PCA

import src.data_statistics as data_statistics
import src.my_clustering as my_clustering

from src.word_vector_analysis import visualize_clustering_results

ubuntu_index = 2
use_gaussian = True
loaded_vocabulary = False
sorted_kb_vocabulary = []
kb_vocabulary = {}
counts = []
navigate_words = {"where": 1, "store": 3, "university": 4, "navigate": 5, "navigating":5, "station": 5, "gas": 5, "park": 5,
                  "parking": 5, "traffic": 5, "fastest": 1, "road": 5, "closest": 5, "nearest": 5, "shortest": 5,
                  "directions": 5, "route": 5, "oval": 5, "dish": 5, "coffee": 3, "tea": 3, "hotel": 5, "mall": 5}
schedule_words = {"when": 1, "with": 1, "schedule": 5, "appointment": 8, "date": 5, "activity": 5, "pm": 3, "am": 3,
                  "remind": 6, "reminder": 6, "set": 1, "football": 4, "swimming": 4, "conference": 4, "time": 4}
weather_words = {"how": 1, "what": 1, "weather": 5, "temperature": 5, "rain": 5, "cloud": 5, "low": 2, "high": 2,
                 "forecast": 5, "clear": 1}
ubuntu_words = {"ubuntu": 10, "windows": 4, "linux": 10, "install": 5, "sudo": 5, "help": 3, "graphic": 5, "audio": 5, "boot": 4,
                "unity": 4, "nvidia": 5, "ati": 5, "display": 3, "program": 3, "gnome": 3, "debian": 5, "hardware": 5, "login": 5, "network": 5, "app": 5}


def load_kb_vocabulary():
    global loaded_vocabulary
    global sorted_kb_vocabulary
    global kb_vocabulary
    global navigate_words, weather_words, schedule_words
    kb_vocab_file_name = "../../data/kvret_entities_cleaning.json"
    with open(kb_vocab_file_name, 'r', encoding='utf-8') as f:
        loaded_file = json.load(f)
    for tag in loaded_file:
        print(tag)
        if tag == "poi":
            for poi in loaded_file[tag]:
                navigate_words[poi["address"]] = 5
                navigate_words[poi["poi"]] = 5
                navigate_words[poi["type"]] = 5
        elif tag == "distance" or tag == "poi_type" or tag == "traffic_info":
            for real_tag in loaded_file[tag]:
                navigate_words[real_tag] = 5
        elif tag == "weather_attribute" or tag == "temperature" or tag == "location" or tag == "weekly_time":
            for real_tag in loaded_file[tag]:
                weather_words[real_tag] = 5
        elif tag == "agenda" or tag == "room" or tag == "party" or tag == "time" or tag == "event":
            for real_tag in loaded_file[tag]:
                schedule_words[real_tag] = 5
        elif tag == "date":
            for real_tag in loaded_file[tag]:
                schedule_words[real_tag] = 5
                weather_words[real_tag] = 3
        else:
            print("WHAT HAPPENED IN KB LOAD???")
    loaded_vocabulary = True


def get_best_cluster(text, index):
    global csv_data
    global counts
    words = [{}, {}, {}, {}]
    if not loaded_vocabulary:
        load_kb_vocabulary()
    words[my_clustering.navigate_index] = navigate_words
    words[my_clustering.schedule_index] = schedule_words
    words[my_clustering.ubuntu_index] = ubuntu_words
    words[my_clustering.weather_index] = weather_words
    scores = [0, 0, 0, 0]
    score_attributes = ["Schedule: ", "Weather: ", "Ubuntu: ", "Navigate: "]
    text = text.lower()
    # text = clean_text.replace_with_tokens(text)
    tokenized_text = word_tokenize(text)
    for dic_index, dic in enumerate(words):
        for word_index, word in enumerate(tokenized_text):
            if dic.__contains__(word):
                scores[dic_index] += dic[word]
                score_attributes[dic_index] += word + ": " + str(dic[word]) + ", "
            # 2-grams
            if word_index < len(tokenized_text) - 1:
                concatenated_word = tokenized_text[word_index] + " " + tokenized_text[word_index + 1]
                if dic.__contains__(concatenated_word):
                    print("Found 2-gram!")
                    scores[dic_index] += dic[concatenated_word]
                    score_attributes[dic_index] += concatenated_word + ": " + str(dic[concatenated_word]) + ", "
            # 3-grams
            if word_index < len(tokenized_text) - 2:
                concatenated_word = tokenized_text[word_index] + " " + tokenized_text[word_index + 1] + " "\
                                    + tokenized_text[word_index + 2]
                if dic.__contains__(concatenated_word):
                    print("Found 3-gram!")
                    scores[dic_index] += dic[concatenated_word]
                    score_attributes[dic_index] += concatenated_word + ": " + str(dic[concatenated_word]) + ", "
            # 4-grams
            if word_index < len(tokenized_text) - 3:
                concatenated_word = tokenized_text[word_index] + " " + tokenized_text[word_index + 1] + " "\
                                    + tokenized_text[word_index + 2] + " " + tokenized_text[word_index + 3]
                if dic.__contains__(concatenated_word):
                    print("Found 4-gram!")
                    scores[dic_index] += dic[concatenated_word]
                    score_attributes[dic_index] += concatenated_word + ": " + str(dic[concatenated_word]) + ", "
    counts.append(scores)
    for attr_index, attr in enumerate(score_attributes):
        if attr_index == 0:
            csv_data.set_value(index, 'x', attr)
        else:
            csv_data.set_value(index, 'x', str(csv_data.loc[index]['x']) + attr)
    max_indexes = [index for index, x in enumerate(scores) if x == max(scores)]
    if len(max_indexes) == 1:
        cluster_index = max_indexes[0]
    elif len(max_indexes) < 4:
        random_chosen = random.randint(0, len(max_indexes) - 1)
        cluster_index = max_indexes[random_chosen]
    else:
        cluster_index = my_clustering.ubuntu_index
    return cluster_index


if __name__ == '__main__':
    mode = ["test", "train"]
    date = ["20-09-2018", "18-09-2018"]
    time = ["16-41", "16-00"]
    choose = 1
    mode = mode[choose]
    date = date[choose]
    time = time[choose]
    results_dir = "results-server"
    best = "BEST - "
    cluster_dir_name = best + "kvret_train_public with tf-idf - " + date + " " + time + "/"
    ubuntu_data = pandas.DataFrame(
        my_clustering.clean_words((my_clustering.read_from_csv(mode + "-new-converted-min", True))),
        columns=["clean_sum", "sum"])
    file_name = "kvret_" + mode + "_public"
    with open("../../data/" + file_name + '.json') as json_file:
        json_data = json.load(json_file)
    print("START TO CLEAN WORDS")
    if mode == "test":
        data_statistics.set_true_labels([ubuntu_index] * 100)
    elif mode == "train":
        data_statistics.set_true_labels([ubuntu_index] * 800)
    csv_data = data_statistics.clean_words_json(json_data, file_name, True)
    csv_data = pandas.concat([ubuntu_data, csv_data], axis=0, ignore_index=True, sort=False)
    true_labels = data_statistics.get_true_labels()
    chunk_size = 1
    for i in range(0, len(csv_data), chunk_size):
        clustering_data = csv_data[i:i + chunk_size]
        cluster_nums = [get_best_cluster(csv_data['sum'][k], k) for k in range(i, i + chunk_size)]
        for k in range(i, i + chunk_size):
            csv_data.set_value(k, 'cluster_num', cluster_nums[k - i])
        print(str(i))
    csv_data['actual_cluster'] = pandas.Series(true_labels)
    # save cluster_nums and evaluate
    try:
        read_in_labels = []
        for i in range(0, len(csv_data['cluster_num'])):
            read_in_labels.append(csv_data['cluster_num'][i])
        print(str(len(read_in_labels)))
        print(str(len(true_labels)))
        data_statistics.set_true_labels(true_labels)
        print(str(data_statistics.get_accuracy_score(read_in_labels)))
    except ValueError:
        print("ValueError")
    my_clustering.write_source_to_csv(csv_data, file_name + " - Routing bot - 4 Clusters with tf-idf",
                                      "../../" + results_dir + "/" + cluster_dir_name)
    pca = PCA(n_components=2)
    reduced_counts = pca.fit_transform(counts)
    visualize_clustering_results.set_dialog_colors(true_labels)
    visualize_clustering_results.plot(reduced_counts, "reality - ")
    visualize_clustering_results.set_dialog_colors(read_in_labels)
    visualize_clustering_results.plot(reduced_counts, "prediction - ")
