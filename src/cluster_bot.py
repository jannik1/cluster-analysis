from __future__ import unicode_literals

import itertools
import os
import pickle
import re

import nltk
import numpy as np
import logging

import pandas
import tensorflow as tf
from keras import Sequential
from keras.callbacks import ModelCheckpoint
from keras.layers import Embedding, LSTM, Dropout, TimeDistributed, Dense, Activation
from keras.optimizers import Adam
from keras.utils import to_categorical
from nltk import TextCollection, word_tokenize

data_path = "../../data/train_results"
train_dir_name = "../../data/train_results"
train_file_name = "train_data"
val_file_name = ""
# chunk_size = 100
# vocab = {"<unk>": 0, "<tab>": 1, ".": 2, "?": 3, "!": 4, "<eos>": 5}
UNK = 'unk'
# EPOCHS = 30
# TRAIN_BATCH_SIZE = 100
num_steps = 30
batch_size = 20
num_epochs = 50


class KerasBatchGenerator(object):

    def __init__(self, data, num_steps, batch_size, vocabulary, skip_step=5):
        self.data = data
        self.num_steps = num_steps
        self.batch_size = batch_size
        self.vocabulary = vocabulary
        # this will track the progress of the batches sequentially through the
        # data set - once the data reaches the end of the data set it will reset
        # back to zero
        self.current_idx = 0
        # skip_step is the number of words which will be skipped before the next
        # batch is skimmed from the data set
        self.skip_step = skip_step

    def generate(self):
        x = np.zeros((self.batch_size, self.num_steps))
        y = np.zeros((self.batch_size, self.num_steps, self.vocabulary))
        while True:
            for i in range(self.batch_size):
                if self.current_idx + self.num_steps >= len(self.data):
                    # reset the index back to the start of the data set
                    self.current_idx = 0
                x[i, :] = self.data[self.current_idx:self.current_idx + self.num_steps]
                temp_y = self.data[self.current_idx + 1:self.current_idx + self.num_steps + 1]
                # convert all of temp_y into a one hot representation
                y[i, :, :] = to_categorical(temp_y, num_classes=self.vocabulary)
                self.current_idx += self.skip_step
            yield x, y


def get_logger(file_name):
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(file_name)

    return logger


_logger = get_logger(__name__)


def word_to_index(word):
    if word2index.__contains__(word):
        return word2index[word]
    return word2index[UNK]


def index_to_word(index):
    if index2word.__contains__(index):
        return index2word[index]
    raise IndexError


def tokenize(dialogs):
    tokenized_dialogs = []
    dialogs = [dialog if dialog == dialog else "NaN" for dialog in dialogs]
    for index, dialog in enumerate(dialogs):
        tokenized_dialog = word_tokenize(dialog)
        if len(tokenized_dialog) < 29:
            for i in range(29 - len(tokenized_dialog)):
                tokenized_dialog.insert(0, "<tab>")
        if len(tokenized_dialog) > 29:
            tokenized_dialog = tokenized_dialog[0:29]
        if tokenized_dialog[len(tokenized_dialog) - 1] == "." or tokenized_dialog[len(tokenized_dialog) - 1] == "?"\
                or tokenized_dialog[len(tokenized_dialog) - 1] == "!":
            tokenized_dialog.append(tokenized_dialog[len(tokenized_dialog) - 1])
        else:
            tokenized_dialog.append("<eos>")
        tokenized_dialogs.append(tokenized_dialog)
    return tokenized_dialogs


def get_input():
    global train_data
    global train_dialogs_input
    global train_dialogs_output
    global tokenized_train_input
    global tokenized_train_output
    train_data = pandas.read_csv(train_dir_name + "/" + train_file_name + ".csv", encoding="ISO-8859-1", sep=';')
    train_data.fillna("")
    train_dialogs_input = train_data['input']
    train_dialogs_output = train_data['output']
    tokenized_train_input = tokenize(train_dialogs_input)
    tokenized_train_output = tokenize(train_dialogs_output)


def get_vectors(dialogs):
    vectors = []
    for index, dialog in enumerate(dialogs):
        vector = []
        tokenized_dialog = word_tokenize(dialog)
        if len(tokenized_dialog) < 29:
            for i in range(29 - len(tokenized_dialog)):
                vector.insert(0, word_to_index("<tab>"))
        for word_index, word in enumerate(tokenized_dialog):
            if word_index < 29:
                vector.append(word_to_index(word))
        if dialog[len(dialog) - 1] == "." or dialog[len(dialog) - 1] == "?"\
                or dialog[len(dialog) - 1] == "!":
            vector.append(word_to_index(dialog[len(dialog) - 1]))
        else:
            vector.append(word_to_index("<eos>"))
        vectors.append(vector)
        if index % 100 == 0:
            print(str(index))
    return vectors


def convert_input_to_vectors():
    global train_input_dialog_vectors
    global train_output_dialog_vectors
    global train_dialog_vectors
    global my_dialogs
    if os.path.exists("input_vectors.pk") and os.path.exists("output_vectors.pk"):
        with open("input_vectors.pk", 'rb') as pickle_file:
            train_input_dialog_vectors = pickle.load(pickle_file)
        with open("output_vectors.pk", 'rb') as pickle_file:
            train_output_dialog_vectors = pickle.load(pickle_file)
    else:
        train_inputs = [dialog if dialog == dialog else "NaN" for dialog in train_dialogs_input]
        train_outputs = [dialog if dialog == dialog else "NaN" for dialog in train_dialogs_output]
        train_inputs_plus_outputs = train_inputs + train_outputs
        my_dialogs = TextCollection(train_inputs_plus_outputs)
        # for i in range(no_iterations):
        train_input_dialog_vectors = get_vectors(train_inputs)
        with open('input_vectors.pk', 'wb') as fin:
            pickle.dump(train_input_dialog_vectors, fin)
        train_output_dialog_vectors = get_vectors(train_outputs)
        with open('output_vectors.pk', 'wb') as fin:
            pickle.dump(train_output_dialog_vectors, fin)
    train_dialog_vectors = []
    for index, _ in enumerate(train_input_dialog_vectors):
        train_dialog_vectors.append(train_input_dialog_vectors[index])
        train_dialog_vectors.append(train_output_dialog_vectors[index])


def init_model():
    global model
    global optimizer
    global checkpointer
    global train_data_generator
    global valid_data_generator
    train_data_generator = KerasBatchGenerator(train_dialog_vectors, num_steps, batch_size, vocabulary,
                                               skip_step=num_steps)
    valid_data_generator = KerasBatchGenerator(valid_data, num_steps, batch_size, vocabulary,
                                               skip_step=num_steps)

    hidden_size = 500
    use_dropout=True
    model = Sequential()
    model.add(Embedding(vocabulary, hidden_size, input_length=num_steps))
    model.add(LSTM(hidden_size, return_sequences=True))
    model.add(LSTM(hidden_size, return_sequences=True))
    if use_dropout:
        model.add(Dropout(0.5))
    model.add(TimeDistributed(Dense(vocabulary)))
    model.add(Activation('softmax'))

    optimizer = Adam()

    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['categorical_accuracy'])
    checkpointer = ModelCheckpoint(filepath=data_path + '/model-{epoch:02d}.hdf5', verbose=1)


def train_model():
    model.fit_generator(train_data_generator.generate(), len(train_dialog_vectors)//(batch_size*num_steps), num_epochs,
                        validation_data=valid_data_generator.generate(),
                        validation_steps=len(valid_data)//(batch_size*num_steps), callbacks=[checkpointer])
    model.save(data_path + "final_model.hdf5")


def predict_with_model():
    # Evaluation step(generating text using the model learned)

    # number of characters to generate
    num_generate = 1000

    # You can change the start string to experiment
    start_string = 'Q'
    # converting our start string to numbers(vectorizing!)
    input_eval = [word2index[word] for word in start_string]
    input_eval = tf.expand_dims(input_eval, 0)

    # empty string to store our results
    text_generated = ''

    # low temperatures results in more predictable text.
    # higher temperatures results in more surprising text
    # experiment to find the best setting
    temperature = 1.0

    # hidden state shape == (batch_size, number of rnn units); here batch size == 1
    hidden = [tf.zeros((1, units))]
    for i in range(num_generate):
        prediction = model.predict("Can you find me a Chinese restaurant that is within 5 miles?")
        print(str(prediction))
        predictions, hidden = model(input_eval, hidden)
        predicted_sequence = []
        for prediction_vector in predictions:
            next_index = np.argmax(prediction_vector)
            next_token = index_to_word(next_index)
            predicted_sequence.append(next_token)
        print(str(' '.join(predicted_sequence)))
        # using a multinomial distribution to predict the word returned by the model
        predictions = predictions / temperature
        predicted_id = tf.multinomial(tf.exp(predictions), num_samples=1)[0][0].np()

        # We pass the predicted word as the next input to the model
        # along with the previous hidden state
        input_eval = tf.expand_dims([predicted_id], 0)

        try:
            char_generated = index_to_word(predicted_id)
        except:
            char_generated = "<gen_unk>"
        text_generated += char_generated + " "

    text_generated = re.sub(r"\.", ".\n", text_generated)
    text_generated = re.sub(r"!", "!\n", text_generated)
    text_generated = re.sub(r"\?", "?\n", text_generated)
    text_generated = re.sub("<eos>", "<eos>\n", text_generated)
    print(start_string + text_generated)


def init():
    global index2word
    global word2index
    # get frequency distribution
    freq_dist = nltk.FreqDist(itertools.chain(*tokenized_train_input))

    # get vocabulary of 'vocab_size' most used words

    vocab = freq_dist.most_common(vocab_size)
    # index2word

    index2word = ['_'] + [UNK] + [x[0] for x in vocab]
    # word2index

    word2index = dict([(w, i) for i, w in enumerate(index2word)])
    return index2word, word2index, freq_dist


if __name__ == '__main__':
    vocab_size = 1000
    embedding_dim = 30
    units = 1024
    BATCH_SIZE = 1
    BUFFER_SIZE = 100

    get_input()
    init()
    convert_input_to_vectors()
    # for index, vector in enumerate(train_input_dialog_vectors):
    #     train_input_dialog_vectors[index] = [int((value + 5) * 1000) for value in vector]
    # for index, vector in enumerate(train_output_dialog_vectors):
    #     train_output_dialog_vectors[index] = [int((value + 5) * 1000) for value in vector]
    vocabulary = vocab_size
    init_model()
    # dataset = tf.data.Dataset.from_tensor_slices(
    #     (np.asarray(train_input_dialog_vectors), np.asarray(train_output_dialog_vectors))).shuffle(BUFFER_SIZE)
    # dataset = dataset.apply(tf.contrib.data.batch_and_drop_remainder(BATCH_SIZE))
    train_model()
    # predict_with_model()
