import re
import datetime
import os
import json
import pandas
import string


from nltk.text import TextCollection
from nltk.tokenize import word_tokenize
from nltk.stem.snowball import SnowballStemmer
from nltk.stem import WordNetLemmatizer
from sklearn.feature_extraction import stop_words
from sklearn import metrics

from src import clean_text

to_fill_textcollection = True
now = datetime.datetime.now()
global_stop_words = []
for stop_word in stop_words.ENGLISH_STOP_WORDS:
    global_stop_words.append(stop_word)
SCRIPTED = False
LOG = False
read_in_labels = []
cluster_file = pandas.DataFrame()
ubuntu_index = 2
weather_index = 1
schedule_index = 0
navigate_index = 3
true_labels = []


def read_from_csv(csv_file_name):
    global true_labels
    if csv_file_name == "kvret_train_public":
        nrows = 1000
        csv_file_name = "train-new-converted"
    else:
        nrows = 100
        csv_file_name = "test-new-converted"
    if csv_file_name == "train-full-converted" or csv_file_name == "train-new-converted"\
        or csv_file_name == "train-converted":
        label_name = "Utterance"
    else:
        label_name = "Ground Truth Utterance"
    true_labels = [ubuntu_index] * nrows
    complete_data = pandas.read_csv("../data/" + csv_file_name + ".csv", encoding="ISO-8859-1", sep=';')
    for index, row in enumerate(complete_data["Context"]):
        if complete_data["Label"][index] == 1:
            complete_data.set_value("Context", index, complete_data.loc[index]["Context"]
                                    + complete_data.loc[index][label_name])
    data_subset = pandas.DataFrame()
    dialog_number = 0
    while len(data_subset) < nrows:
        if dialog_number >= len(complete_data):
            print("Stopped clustering because file contained not enough utterances of 'ubuntu'")
            exit()
        tokenized_dialog = word_tokenize(complete_data["Context"][dialog_number])
        tokenized_utterance = word_tokenize(complete_data[label_name][dialog_number])
        tokenized_dialog.extend(tokenized_utterance)
        if 8 < len(tokenized_dialog) <= 120 and\
                (tokenized_dialog.__contains__("nvidia")
                or tokenized_dialog.__contains__("ati")
                or tokenized_dialog.__contains__("graphics")
                or tokenized_dialog.__contains__("network")
                or tokenized_dialog.__contains__("ubuntu")):
            complete_data.set_value("Context", dialog_number, complete_data.loc[dialog_number]["Context"]
                                    + complete_data.loc[dialog_number][label_name])
            data_subset = data_subset.append(complete_data.loc[dialog_number], ignore_index=True)
        dialog_number += 1
    return data_subset


def get_time():
    return now.strftime("%d-%m-%Y %H-%M")


def get_user_input():
    global file_name
    file_choice = int(input("What data? 0 = train-new, 1 = test-new, 2 = valid-new, 3 = train, "
                            "4 = test-1-distractor, 5 = test-1-distractor-first-100, 6 = kvret_test_public, "
                            "7 = kvret_train_public, 8 = kvret_dev_public. Other input = exit\n"))
    possible_file_choices = ["train-new", "test-new", "valid-new", "train", "test-1-distractor",
                             "test-1-distractor-first-100", "kvret_test_public", "kvret_train_public",
                             "kvret_dev_public"]
    if len(possible_file_choices) > file_choice >= 0:
        file_name = possible_file_choices[file_choice]
    else:
        exit()
    print(file_name)


def do_with_time_output(f, *args):
    start_time = datetime.datetime.now()
    result = []
    if len(args) == 1:
        result = f(args[0])
    elif len(args) == 2:
        result = f(args[0], args[1])
    time_elapsed = datetime.datetime.now() - start_time
    print('Time elapsed (hh:mm:ss.ms) {}'.format(time_elapsed))
    return result


def clean_words(data):
    data = data.fillna('NULL')
    data['clean_sum'] = data["Context"].apply(lambda sentence: clean_sentence(sentence))
    return data


def clean_words_json(data, local_file_name="", only_first_utterance=False):
    global true_labels
    global file_name
    if local_file_name != "":
        file_name = local_file_name
    returned_data = pandas.DataFrame(columns=['sum', 'clean_sum', 'Full_Context', 'cluster_num', 'distance', 'x', 'y', ],
                                     index=[i for i in range(len(data))])
    returned_data = returned_data.fillna('NULL')
    for dialog_index, dialog in enumerate(data):
        if dialog['scenario']['task']['intent'] == "weather":
            if file_name == "kvret_train_public":
                true_labels.append(weather_index)
            else:
                true_labels.append(weather_index)
        elif dialog['scenario']['task']['intent'] == "navigate":
            if file_name == "kvret_train_public":
                true_labels.append(navigate_index)
            else:
                true_labels.append(navigate_index)
        elif dialog['scenario']['task']['intent'] == "schedule":
            if file_name == "kvret_train_public":
                true_labels.append(schedule_index)
            else:
                true_labels.append(schedule_index)
        for utterance_index, utterance in enumerate(dialog['dialogue']):
            if not only_first_utterance or utterance_index == 0:
                if str(returned_data['clean_sum'][dialog_index]) != "NULL":
                    returned_data.loc[dialog_index]['sum'] = str(returned_data['sum'][dialog_index]) + " "\
                                                             + utterance['data']['utterance']
                    returned_data.loc[dialog_index]['clean_sum'] = str(returned_data['clean_sum'][dialog_index]) + " "\
                        + clean_sentence(utterance['data']['utterance'])
                else:
                    returned_data.loc[dialog_index]['clean_sum'] = clean_sentence(utterance['data']['utterance'])
                    returned_data.loc[dialog_index]['sum'] = utterance['data']['utterance']
            if only_first_utterance:
                if str(returned_data['Full_Context'][dialog_index]) != "NULL":
                    returned_data.loc[dialog_index]['Full_Context'] = str(returned_data['Full_Context'][dialog_index]) + " "\
                                                             + utterance['data']['utterance']
                else:
                    returned_data.loc[dialog_index]['Full_Context'] = utterance['data']['utterance']
    return returned_data


def clean_words_json_last_utterances(data, local_file_name=""):
    global true_labels
    global file_name
    if local_file_name != "":
        file_name = local_file_name
    returned_data = pandas.DataFrame(columns=['driver', 'car', 'cluster_num', 'distance', 'x', 'y', ],
                                     index=[i for i in range(len(data))])
    returned_data = returned_data.fillna('NULL')
    for dialog_index, dialog in enumerate(data):
        if dialog['scenario']['task']['intent'] == "weather":
            if file_name == "kvret_train_public":
                true_labels.append(weather_index)
            else:
                true_labels.append(weather_index)
        elif dialog['scenario']['task']['intent'] == "navigate":
            if file_name == "kvret_train_public":
                true_labels.append(navigate_index)
            else:
                true_labels.append(navigate_index)
        elif dialog['scenario']['task']['intent'] == "schedule":
            if file_name == "kvret_train_public":
                true_labels.append(schedule_index)
            else:
                true_labels.append(schedule_index)
        for utterance_index, utterance in enumerate(dialog['dialogue']):
            if utterance_index == len(dialog['dialogue']) - 1:
                returned_data.loc[dialog_index]['car'] = utterance['data']['utterance']
            elif utterance_index == len(dialog['dialogue']) - 2:
                returned_data.loc[dialog_index]['driver'] = utterance['data']['utterance']
    return returned_data


def remove_apostrophes_typos(sentence):
    list_of_abbrvs = [("can", "t", "cannot"), ("didn", "t", "did not"), ("doesn", "t", "does not"),
                      ("isn", "t", "is not"), ("won", "t", "will not"), ("wouldn", "t", "would not"),
                      ("i", "ll", "i will"), ("you", "ll", "you will"), ("i", "m", "i am"), ("i", "ve", "i have"),
                      ("i", "d", "i would"), ("you", "d", "you would"), ("aren", "t", "are not")]
    for (prefix, suffix, repl) in list_of_abbrvs:
        sentence = re.sub(r'\b' + prefix + r'[@\-.,`"\';#?]*' + suffix + r'\b', repl, sentence)
    sentence = sentence.replace('anyon', 'anyone')
    sentence = sentence.replace('anyonee', 'anyone')
    sentence = sentence.replace('oubuntu', 'ubuntu')
    sentence = sentence.replace('ubunut', 'ubuntu')
    return sentence


def remove_www(sentence):
    sentence = re.sub(r'http\S+', '', sentence)
    sentence = re.sub(r'www.\S+', '', sentence)
    sentence = re.sub(r'\S+\.\S+\.(com|org|de)\S+', '', sentence)
    return sentence


def remove_special_chars(sentence):
    sentence = re.sub('__eou__', '', sentence)
    sentence = re.sub('__eot__', '', sentence)
    sentence = re.sub('[\t,.:;_=+*~^\-"!?@%&#$`/|\'()[\]{}]', ' ', sentence)
    return sentence


def stem_sentence(sentence):
    lemmatizer = WordNetLemmatizer()
    sentence = lemmatizer.lemmatize(sentence, "v")
    snowball_stemmer = SnowballStemmer("english")
    sentence = snowball_stemmer.stem(sentence)
    return sentence


def clean_sentence(sentence):
    # old_sentence = sentence
    sentence = sentence.lower()
    sentence = clean_text.replace_with_tokens(sentence)
    # sentence = remove_apostrophes_typos(sentence)
    # sentence = re.sub(r'[0123456789]*', '', sentence)
    # # sentence = remove_www(sentence)
    # sentence = remove_special_chars(sentence)
    # sentence = stem_sentence(sentence)
    # sentence_tokenized = word_tokenize(sentence)
    # filter(lambda x: x not in global_stop_words, sentence_tokenized)
    # sentence = "".join([" " + i if not i.startswith("'") and i not in string.punctuation else i
    #                     for i in sentence_tokenized]).strip()
    # if LOG and sentence_tokenized.__contains__("im"):
    #     print("SHEEESH")
    # if LOG and len(sentence_tokenized) < 3:
    #     print("BEFORE: " + old_sentence + "\n AFTER: " + sentence + "\n")
    return sentence


def get_accuracy_score(local_read_in_labels):
    return metrics.accuracy_score(y_pred=local_read_in_labels, y_true=true_labels)


def get_cluster_file():
    return cluster_file


def set_cluster_file(local_cluster_file):
    global cluster_file
    cluster_file = local_cluster_file


def set_true_labels(local_true_labels):
    global true_labels
    true_labels = local_true_labels


def get_true_labels():
    return true_labels


def clean_a_bit(dialog):
    dialog = re.sub('__eou__', '', dialog)
    dialog = re.sub('__eot__', '', dialog)
    return stem_sentence(re.sub('[\t,.:;_=+*<>~^\-"!?@%&#$`/|\'()[\]{}]', ' ',
                         remove_apostrophes_typos(dialog.lower())))


def print_word_statistics(data):
    for dialog_type in range(0, 4):
        dialog_lengths = []
        read_words = []
        word_counts = []
        dialogs = []
        for index, word_list in enumerate(data):
            if true_labels[index] == dialog_type:
                dialog_lengths.append(len(word_list))
                dialogs.append(clean_a_bit(csv_data['sum'][index]))
                for word in word_list:
                    if read_words.__contains__(word):
                        word_index = read_words.index(word)
                        word_counts[word_index] += 1
                    else:
                        read_words.append(word)
                        word_counts.append(1)
        textcollection = TextCollection(dialogs)
        word_statistics = []
        for index, word in enumerate(read_words):
            word_statistics.append((word, word_counts[index], textcollection.idf(word)))
        word_statistics.sort(key=lambda word_statistic: word_statistic[1], reverse=True)
        word_statistics = pandas.DataFrame(word_statistics)
        word_statistics.to_csv("word_statistics-" + str(dialog_type) + ".csv")
        pandas.DataFrame(dialog_lengths).to_csv("dialog-lengths-" + str(dialog_type) + ".csv")


def start_main(same_file):
    global read_in_labels
    global cluster_file
    global csv_data
    get_user_input()
    full_ubuntu_data = read_from_csv(file_name)
    ubuntu_data = pandas.DataFrame(full_ubuntu_data, columns=["Context"])
    ubuntu_data.rename(columns={"Context": "sum"}, inplace=True)
    json_file = open("../data/" + file_name + '.json')
    json_data = json.load(json_file)
    csv_data = do_with_time_output(clean_words_json, json_data)
    csv_data = pandas.concat([ubuntu_data, csv_data], axis=0, ignore_index=True, sort=False)
    # if same_file:
    #     cluster_file_name = input("Please tell the name of the cluster file\n")
    # else:
    #     cluster_file_name = "kvret_train_public with tf-idf - 06-08-2018 16-50/kvret_train_public -" \
    #                         " Attributes of 4 Clusters with tf-idf - 06-08-2018 16-50.csv"
    # cluster_file = pandas.read_csv("../results/" + cluster_file_name, encoding="ISO-8859-1", sep=';')
    # if same_file:
    #     for dialog in cluster_file['cluster_num']:
    #         read_in_labels.append(dialog)
    #     print(str(get_accuracy_score(read_in_labels)))
    # TODO: print length of dialogs [median, standard derivation, verteilung, differentiate by cluster
    words = [word_tokenize(
        clean_a_bit(dialog)
    ) for dialog in csv_data['sum']]
    print_word_statistics(words)


if __name__ == '__main__':
    start_main(True)
