import csv

import pandas
import pickle

if __name__ == '__main__':
    # train_file_name = raw_input("Please type in name of train file\n")
    # test_file_name = raw_input("Please type in name of test file\n")
    # val_file_name = raw_input("Please type in name of val file\n")
    # train_file = pandas.read_csv("../../data/" + train_file_name, encoding="ISO-8859-1", sep=';')
    # test_file = pandas.read_csv("../../data/" + test_file_name, encoding="ISO-8859-1", sep=';')
    # val_file = pandas.read_csv("../../data/" + val_file_name, encoding="ISO-8859-1", sep=';')
    glove_vector = pandas.read_csv("trained data/glove/glove.6B.50d.txt", header=None, delimiter=" ",
                                   quoting=csv.QUOTE_NONE)
    data_columns = [str(i) for i in range(50)]
    data_columns.insert(0, "word")
    glove_vector.columns = data_columns
    word_to_vector = dict()
    for index in range(len(glove_vector)):
        if index % 100 == 0:
            print(str(index))
        word_to_vector[glove_vector["word"][index]] = [glove_vector[str(i)][index] for i in (range(50))]
    with open("trained data/glove/glove.6B.50d-dict.pkl", 'wb') as fin:  # ("../../data/" + "dataset" + '.pkl', 'wb') as fin:
        pickle.dump(word_to_vector, fin)
