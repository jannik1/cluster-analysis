import pandas


def print_alternative_plot(coords, csv_data, no_clusters=4, SHOW=True):
    from matplotlib import pyplot as plt
    from itertools import cycle

    plt.figure(1)
    plt.clf()

    colors = cycle('bgrcmykbgrcmykbgrcmykbgrcmyk')
    generated_colors = [(1, 0, 0, 0.5), (0, 1, 0, 0.5), (0, 0, 1, 0.5), (0, 0, 0, 0.5)]
    for k, col in zip(range(no_clusters), colors):
        my_members = csv_data['cluster_num'] == k
        my_actual_members = csv_data['actual_cluster'] == k
        for i in range(len(csv_data['cluster_num'])):
            if my_members[i]:
                plt.plot(coords[3628 - (404 - i)][0], coords[3628 - (404 - i)][1], col + '.')
                # plt.plot(coords[3628 - (404 - i)][0], coords[3628 - (404 - i)][1], 'o',
                #          color=generated_colors[int(csv_data['actual_cluster'][i])][0:3],
                #          alpha=generated_colors[int(csv_data['actual_cluster'][i])][3], markersize=10)
        # plt.plot(cluster_center[0], cluster_center[1], 'o', markerfacecolor=col,
        #          markeredgecolor='k', markersize=14)
    plt.title('Estimated number of clusters: %d' % no_clusters)
    plt.savefig("AAAAAAAA - alternative plot - " + str(no_clusters) + " clusters - " + ".pdf", width=1920, height=1080)
    if SHOW:
        plt.show()
    else:
        plt.clf()


def main():
    results_dir = "results-server"
    cluster_dir_name = "BEST - kvret_train_public with tf-idf - 20-09-2018 16-41"
    cluster_file_name = "/kvret_test_public - Routing bot - 4 Clusters with tf-idf - 24-09-2018 15-34.csv"
    csv_data = pandas.read_csv("../../" + results_dir + "/" + cluster_dir_name + cluster_file_name, encoding="ISO-8859-1",
                               sep=';')
    coords = []
    for i, x in enumerate(csv_data['x'].iteritems()):
        coords.append((csv_data['x'][i], csv_data['y'][i]))
    print_alternative_plot(coords, csv_data)


if __name__ == '__main__':
    main()
