from __future__ import unicode_literals

import itertools
import json
import os
import pickle
import random
import re

import nltk
import numpy
import time
import logging

import pandas
import tensorflow as tf
import seq2seq
from seq2seq.models import AttentionSeq2Seq

from nltk import TextCollection, word_tokenize
from sklearn.decomposition import PCA, TruncatedSVD
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline, FeatureUnion
from tensorflow.contrib import rnn

from src import my_clustering, data_statistics, clean_text
from src.my_clustering import ubuntu_index

mode = "full"
train_dir_name = "../../KVMemnn-jannik/data/"
train_file_name = "train_data - " + mode
test_file_name = "test_data - " + mode
chunk_size = 100
# vocab = {"<unk>": 0, "<tab>": 1, ".": 2, "?": 3, "!": 4, "<eos>": 5}
UNK = 'unk'
EPOCHS = 30
TRAIN_BATCH_SIZE = 100
tf_idf = False
vocab_size = 4000
# embedding_dim = 30
units = 16
# BATCH_SIZE = 1
# BUFFER_SIZE = 100
to_show_in_browser = False


def get_train_input_dialog_vectors():
    return train_input_dialog_vectors


def get_train_output_dialog_vectors():
    return train_output_dialog_vectors


def get_test_input_dialog_vectors():
    return test_input_dialog_vectors


def get_test_output_dialog_vectors():
    return test_output_dialog_vectors


def get_logger(file_name):
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(file_name)

    return logger


_logger = get_logger(__name__)


def word_to_index(word):
    if word2index.__contains__(word):
        return word2index[word]
    return word2index[UNK]


def index_to_word(index):
    if index2word.__contains__(index):
        return index2word[index]
    raise IndexError


# using sparse_softmax_cross_entropy so that we don't have to create one-hot vectors
def loss_function(real, preds):
    return tf.losses.sparse_softmax_cross_entropy(labels=real, logits=preds)


def tokenize(dialogs):
    tokenized_dialogs = []
    dialogs = [dialog if dialog == dialog else "NaN" for dialog in dialogs]
    for index, dialog in enumerate(dialogs):
        tokenized_dialog = word_tokenize(dialog)
        if len(tokenized_dialog) < 29:
            for i in range(29 - len(tokenized_dialog)):
                tokenized_dialog.insert(0, "<tab>")
        if len(tokenized_dialog) > 29:
            tokenized_dialog = tokenized_dialog[0:29]
        if tokenized_dialog[len(tokenized_dialog) - 1] == "." or tokenized_dialog[len(tokenized_dialog) - 1] == "?" \
                or tokenized_dialog[len(tokenized_dialog) - 1] == "!":
            tokenized_dialog.append(tokenized_dialog[len(tokenized_dialog) - 1])
        else:
            tokenized_dialog.append("<eos>")
        tokenized_dialogs.append(tokenized_dialog)
    return tokenized_dialogs


def get_input():
    global train_data
    global train_dialogs_input
    global train_dialogs_output
    global tokenized_train_input
    global tokenized_train_output
    ubuntu_data = pandas.DataFrame(my_clustering.clean_words((my_clustering.read_from_csv("train-new-converted-min", True))),
                                   columns=["clean_sum", "sum"])
    file_name = "kvret_train_public"
    with open("../../data/" + file_name + '.json') as json_file:
        json_data = json.load(json_file)
    data_statistics.set_true_labels([ubuntu_index] * 800)
    train_data = data_statistics.clean_words_json(json_data, file_name, True)
    train_data = pandas.concat([ubuntu_data, train_data], axis=0, ignore_index=True, sort=False)
    train_data.fillna("")
    train_dialogs_input = [clean_text.replace_with_tokens(dialog) for dialog in train_data['sum']]
    train_dialogs_output = data_statistics.get_true_labels()
    tokenized_train_input = tokenize(train_dialogs_input)
    tokenized_train_output = [[train_dialogs_output[i]] for i in range(len(train_dialogs_output))]


def get_test_input():
    global test_data
    global test_dialogs_input
    global test_dialogs_output
    global tokenized_test_input
    global tokenized_test_output
    ubuntu_data = pandas.DataFrame(my_clustering.clean_words((my_clustering.read_from_csv("test-new-converted-min", True))),
                                   columns=["clean_sum", "sum"])
    file_name = "kvret_test_public"
    with open("../../data/" + file_name + '.json') as json_file:
        json_data = json.load(json_file)
    data_statistics.set_true_labels([ubuntu_index] * 100)
    test_data = data_statistics.clean_words_json(json_data, file_name, True)
    test_data = pandas.concat([ubuntu_data, test_data], axis=0, ignore_index=True, sort=False)
    test_data.fillna("")
    test_dialogs_input = [clean_text.replace_with_tokens(dialog) for dialog in test_data['sum']]
    test_dialogs_output = data_statistics.get_true_labels()
    tokenized_test_input = tokenize(test_dialogs_input)
    tokenized_test_output = [[test_dialogs_output[i]] for i in range(len(test_dialogs_output))]


def get_vectors(dialogs, fill_up=True):
    vectors = []
    for index, dialog in enumerate(dialogs):
        vector = []
        tokenized_dialog = word_tokenize(str(dialog))
        if len(tokenized_dialog) < 29 and fill_up:
            for i in range(29 - len(tokenized_dialog)):
                vector.insert(0, word_to_index("<tab>"))
        for word_index, word in enumerate(tokenized_dialog):
            if word_index < 29:
                vector.append(word_to_index(word))
        if str(dialog)[len(str(dialog)) - 1] == "." or str(dialog)[len(str(dialog)) - 1] == "?" \
                or str(dialog)[len(str(dialog)) - 1] == "!" and fill_up:
            vector.append(word_to_index(str(dialog)[len(str(dialog)) - 1]))
        elif fill_up:
            vector.append(word_to_index("<eos>"))
        vectors.append(vector)
        if index % 100 == 0:
            print(str(index))
    return vectors


def convert_input_to_vectors():
    global train_input_dialog_vectors
    global train_output_dialog_vectors
    # global my_dialogs
    if os.path.exists("input_vectors.pk") and os.path.exists("output_vectors.pk"):
        with open("input_vectors.pk", 'rb') as pickle_file:
            train_input_dialog_vectors = pickle.load(pickle_file)
        with open("output_vectors.pk", 'rb') as pickle_file:
            train_output_dialog_vectors = pickle.load(pickle_file)
    else:
        train_inputs = [dialog if dialog == dialog else "NaN" for dialog in train_dialogs_input]
        train_outputs = [dialog if dialog == dialog else "NaN" for dialog in train_dialogs_output]
        train_inputs_plus_outputs = train_inputs + train_outputs
        # my_dialogs = TextCollection(train_inputs_plus_outputs)
        # for i in range(no_iterations):
        train_input_dialog_vectors = get_vectors(train_inputs)
        with open('input_vectors.pk', 'wb') as fin:
            pickle.dump(train_input_dialog_vectors, fin)
        train_output_dialog_vectors = get_vectors(train_outputs, fill_up=False)
        with open('output_vectors.pk', 'wb') as fin:
            pickle.dump(train_output_dialog_vectors, fin)


def convert_input_to_tfidf():
    global train_input_dialog_vectors
    global train_output_dialog_vectors
    if os.path.exists("input_vectors_tf_idf.pk") and os.path.exists("output_vectors_tf_idf.pk"):
        with open("input_vectors_tf_idf.pk", 'rb') as pickle_file:
            train_input_dialog_vectors = pickle.load(pickle_file)
        with open("output_vectors_tf_idf.pk", 'rb') as pickle_file:
            train_output_dialog_vectors = pickle.load(pickle_file)
    else:
        max_df = 1.0
        min_df = 5
        vectorizer = Pipeline(steps=[
                    ('union', FeatureUnion([
                        ('word_vec', TfidfVectorizer(analyzer='word', decode_error='strict', binary=True,
                                                     max_df=max_df, min_df=min_df, dtype=numpy.float32,
                                                     ngram_range=(1, 2), sublinear_tf=True)),
                        ('char_vec', TfidfVectorizer(analyzer='char', decode_error='strict', binary=True,
                                                     max_df=max_df, min_df=min_df, dtype=numpy.float32,
                                                     ngram_range=(3, 8), sublinear_tf=True))
                        ])),
                    ])
        vectorizer = vectorizer.fit(train_dialogs_input, train_dialogs_input)
        counts = vectorizer.transform(train_dialogs_input)
        reduced_size = 2
        try:
            pca = PCA(n_components=reduced_size)
            pca = pca.fit(counts.todense())
            reduced_counts = pca.transform(counts.todense())
        except (MemoryError, ValueError):
            print("Using TruncatedSVD because of Memory Error when converting counts to dense array")
            pca = TruncatedSVD(n_components=reduced_size)
            pca = pca.fit(counts.todense())
            reduced_counts = pca.transform(counts)
        train_inputs = reduced_counts
        train_outputs = [dialog if dialog == dialog else "NaN" for dialog in train_dialogs_output]
        train_input_dialog_vectors = []
        for k in range(len(train_inputs)):
            train_input_dialog_vectors.append(27 * [2]) # get_vectors(train_inputs)
        for i, i_vector in enumerate(train_input_dialog_vectors):
            train_input_dialog_vectors[i].append(train_inputs[i][0])
            train_input_dialog_vectors[i].append(train_inputs[i][1])
            train_input_dialog_vectors[i].append(6)
        with open('input_vectors_tf_idf.pk', 'wb') as fin:
            pickle.dump(train_input_dialog_vectors, fin)
        train_output_dialog_vectors = get_vectors(train_outputs, fill_up=False)
        with open('output_vectors_tf_idf.pk', 'wb') as fin:
            pickle.dump(train_output_dialog_vectors, fin)


def convert_test_input_to_vectors():
    global test_input_dialog_vectors
    global test_output_dialog_vectors
    # global my_dialogs
    if os.path.exists("test_input_vectors.pk") and os.path.exists("test_output_vectors.pk"):
        with open("test_input_vectors.pk", 'rb') as pickle_file:
            test_input_dialog_vectors = pickle.load(pickle_file)
        with open("test_output_vectors.pk", 'rb') as pickle_file:
            test_output_dialog_vectors = pickle.load(pickle_file)
    else:
        test_inputs = [dialog if dialog == dialog else "NaN" for dialog in test_dialogs_input]
        test_outputs = [dialog if dialog == dialog else "NaN" for dialog in test_dialogs_output]
        test_inputs_plus_outputs = test_inputs + test_outputs
        # my_dialogs = TextCollection(test_inputs_plus_outputs)
        # for i in range(no_iterations):
        test_input_dialog_vectors = get_vectors(test_inputs)
        with open('test_input_vectors.pk', 'wb') as fin:
            pickle.dump(test_input_dialog_vectors, fin)
        test_output_dialog_vectors = get_vectors(test_outputs, fill_up=False)
        with open('test_output_vectors.pk', 'wb') as fin:
            pickle.dump(test_output_dialog_vectors, fin)


def convert_test_input_to_tfidf():
    global test_input_dialog_vectors
    global test_output_dialog_vectors
    if os.path.exists("test_input_vectors_tf_idf.pk") and os.path.exists("test_output_vectors_tf_idf.pk"):
        with open("test_input_vectors_tf_idf.pk", 'rb') as pickle_file:
            test_input_dialog_vectors = pickle.load(pickle_file)
        with open("test_output_vectors_tf_idf.pk", 'rb') as pickle_file:
            test_output_dialog_vectors = pickle.load(pickle_file)
    else:
        max_df = 1.0
        min_df = 5
        vectorizer = Pipeline(steps=[
                    ('union', FeatureUnion([
                        ('word_vec', TfidfVectorizer(analyzer='word', decode_error='strict', binary=True,
                                                     max_df=max_df, min_df=min_df, dtype=numpy.float32,
                                                     ngram_range=(1, 2), sublinear_tf=True)),
                        ('char_vec', TfidfVectorizer(analyzer='char', decode_error='strict', binary=True,
                                                     max_df=max_df, min_df=min_df, dtype=numpy.float32,
                                                     ngram_range=(3, 8), sublinear_tf=True))
                        ])),
                    ])
        vectorizer = vectorizer.fit(test_dialogs_input, test_dialogs_input)
        counts = vectorizer.transform(test_dialogs_input)
        reduced_size = 2
        try:
            pca = PCA(n_components=reduced_size)
            pca = pca.fit(counts.todense())
            reduced_counts = pca.transform(counts.todense())
        except (MemoryError, ValueError):
            print("Using TruncatedSVD because of Memory Error when converting counts to dense array")
            pca = TruncatedSVD(n_components=reduced_size)
            pca = pca.fit(counts.todense())
            reduced_counts = pca.transform(counts)
        test_inputs = reduced_counts
        test_outputs = [dialog if dialog == dialog else "NaN" for dialog in test_dialogs_output]
        test_input_dialog_vectors = []
        for k in range(len(test_inputs)):
            test_input_dialog_vectors.append(27 * [2])
        for i, i_vector in enumerate(test_input_dialog_vectors):
            test_input_dialog_vectors[i].append(test_inputs[i][0])
            test_input_dialog_vectors[i].append(test_inputs[i][1])
            test_input_dialog_vectors[i].append(6)
        with open('test_input_vectors_tf_idf.pk', 'wb') as fin:
            pickle.dump(test_input_dialog_vectors, fin)
        test_output_dialog_vectors = get_vectors(test_outputs, fill_up=False)
        with open('test_output_vectors_tf_idf.pk', 'wb') as fin:
            pickle.dump(test_output_dialog_vectors, fin)


def init_model():
    global model
    # model = AttentionSeq2Seq(input_dim=5, input_length=7, hidden_dim=10, output_length=8, output_dim=20, depth=4,
    #                          input_shape=(TRAIN_BATCH_SIZE, EPOCHS))
    # model.compile(loss='mse', optimizer='rmsprop')
    from keras import Sequential
    from keras.layers import Dense
    from seq2seq.models import SimpleSeq2Seq

    model = Sequential()
    seq2seq = SimpleSeq2Seq(
        input_dim=vocab_size,
        input_length=30,
        hidden_dim=512,
        output_dim=vocab_size,
        output_length=6,
        depth=1
    )

    model.add(seq2seq)
    model.compile(loss='categorical_crossentropy', optimizer='rmsprop')


# def get_token_vector(token, model):
#     if token in model.vocab:
#         return numpy.array(model[token])
#
#     # return a zero vector for the words that are not presented in the model
#     return numpy.zeros(TOKEN_REPRESENTATION_SIZE)
#
#
# def _sequence_to_vector(sentence, w2v_model):
#     # Here we need predicted vectors only for one sequence, not for the whole batch,
#     # however StatefulRNN works in a such a way that we have to feed predict() function
#     # the same number of examples as in our train batch.
#     # Then we can use only the first predicted sequence and disregard all the rest.
#     # If you have more questions, feel free to address them to https://github.com/farizrahman4u/seq2seq
#     X = numpy.zeros((TRAIN_BATCH_SIZE, INPUT_SEQUENCE_LENGTH, TOKEN_REPRESENTATION_SIZE))
#
#     for t, token in enumerate(sentence):
#         X[0, t] = get_token_vector(token, w2v_model)
#
#     return X
#
#
# def _predict_sequence(input_sequence, nn_model, w2v_model, index_to_token, diversity):
#     input_sequence = input_sequence[:INPUT_SEQUENCE_LENGTH]
#
#     X = _sequence_to_vector(input_sequence, w2v_model)
#     predictions = nn_model.predict(X, verbose=0)[0]
#     predicted_sequence = []
#
#     for prediction_vector in predictions:
#         next_index = numpy.argmax(prediction_vector)
#         next_token = index_to_token[next_index]
#         predicted_sequence.append(next_token)
#
#     return predicted_sequence
#
#
# def predict_sentence(sentence, nn_model, w2v_model, index_to_token, diversity=0.5):
#     input_sequence = tokenize(sentence + ' ' + EOS_SYMBOL)
#     tokens_sequence = _predict_sequence(input_sequence, nn_model, w2v_model, index_to_token, diversity)
#     predicted_sentence = ' '.join(tokens_sequence)
#
#     return predicted_sentence
#
#
# def log_predictions(sentences, nn_model, w2v_model, index_to_token, stats_info=None):
#     for sent in sentences:
#         prediction = predict_sentence(sent, nn_model, w2v_model, index_to_token)
#         _logger.info('[%s] -> [%s]' % (sent, prediction))


def train_model():
    for epoch in range(EPOCHS):
        start = time.time()

        # initializing the hidden state at the start of every epoch
        # hidden = model.reset_states()

        for batch, input in enumerate(train_input_dialog_vectors, TRAIN_BATCH_SIZE):
            # with tf.GradientTape() as tape:
            # dataset_part = dataset[batch:batch + TRAIN_BATCH_SIZE]
            inputs = train_input_dialog_vectors[batch:batch + TRAIN_BATCH_SIZE]
            targets = train_output_dialog_vectors[batch:batch + TRAIN_BATCH_SIZE]
            model.fit(numpy.asarray(inputs), numpy.asarray(targets), batch_size=TRAIN_BATCH_SIZE, nb_epoch=1, verbose=1)
            # target = train_input_dialog_vectors[batch]
            # feeding the hidden state back into the model
            # This is the interesting step
            # predictions, hidden = model(inp, hidden)

            # reshaping the target because that's how the
            # loss function expects it
            # target = tf.reshape(target, (-1,))
            # loss = loss_function(target, predictions)

            # grads = tape.gradient(loss, model.variables)

            if batch % 100 == 0:
                print('Epoch {} Batch {}'.format(epoch + 1, batch))
                # log_predictions(test_sentences, model, w2v_model, index_to_token)
                model.save_weights("seq2seq")

        print('Epoch {}'.format(epoch + 1))
        print('Time taken for 1 epoch {} sec\n'.format(time.time() - start))


def predict_with_model():
    # Evaluation step(generating text using the model learned)

    # number of characters to generate
    num_generate = 1000

    # You can change the start string to experiment
    start_string = 'Q'
    # converting our start string to numbers(vectorizing!)
    input_eval = [word2index[word] for word in start_string]
    input_eval = tf.expand_dims(input_eval, 0)

    # empty string to store our results
    text_generated = ''

    # low temperatures results in more predictable text.
    # higher temperatures results in more surprising text
    # experiment to find the best setting
    temperature = 1.0

    # hidden state shape == (batch_size, number of rnn units); here batch size == 1
    hidden = [tf.zeros((1, units))]
    for i in range(num_generate):
        prediction = model.predict("Can you find me a Chinese restaurant that is within 5 miles?")
        print(str(prediction))
        predictions, hidden = model(input_eval, hidden)
        predicted_sequence = []
        for prediction_vector in predictions:
            next_index = numpy.argmax(prediction_vector)
            next_token = index_to_word(next_index)
            predicted_sequence.append(next_token)
        print(str(' '.join(predicted_sequence)))
        # using a multinomial distribution to predict the word returned by the model
        predictions = predictions / temperature
        predicted_id = tf.multinomial(tf.exp(predictions), num_samples=1)[0][0].numpy()

        # We pass the predicted word as the next input to the model
        # along with the previous hidden state
        input_eval = tf.expand_dims([predicted_id], 0)

        try:
            char_generated = index_to_word(predicted_id)
        except:
            char_generated = "<gen_unk>"
        text_generated += char_generated + " "

    text_generated = re.sub(r"\.", ".\n", text_generated)
    text_generated = re.sub(r"!", "!\n", text_generated)
    text_generated = re.sub(r"\?", "?\n", text_generated)
    text_generated = re.sub("<eos>", "<eos>\n", text_generated)
    print(start_string + text_generated)


def init_dicts():
    global index2word
    global word2index
    # get frequency distribution
    freq_dist = nltk.FreqDist(itertools.chain(*tokenized_train_input))

    # get vocabulary of 'vocab_size' most used words

    vocab = freq_dist.most_common(vocab_size)
    # index2word

    index2word = ['_'] + [UNK] + [x[0] for x in vocab]
    # word2index

    word2index = dict([(w, i) for i, w in enumerate(index2word)])
    return index2word, word2index, freq_dist


start_time = time.time()


def elapsed(sec):
    if sec < 60:
        return str(sec) + " sec"
    elif sec < (60 * 60):
        return str(sec / 60) + " min"
    else:
        return str(sec / (60 * 60)) + " hr"


n_input = 30
# number of units in RNN cell
n_hidden = 512


def RNN(x, weights, biases):
    # reshape to [1, n_input]
    x = tf.reshape(x, [-1, n_input])

    # Generate a n_input-element sequence of inputs
    # (eg. [had] [a] [general] -> [20] [6] [33])
    x = tf.split(x, n_input, 1)

    # 1-layer LSTM with n_hidden units.
    rnn_cell = rnn.BasicLSTMCell(n_hidden)

    # generate prediction
    outputs, states = rnn.static_rnn(rnn_cell, x, dtype=tf.float32)

    # there are n_input outputs but
    # we only want the last output
    return tf.matmul(outputs[-1], weights['out']) + biases['out']


def main():
    # Target log path
    logs_path = '/tmp/tensorflow/rnn_words'
    writer = tf.summary.FileWriter(logs_path)

    vocab_size = len(dictionary)

    # Parameters
    learning_rate = 0.001
    training_iters = 100000
    display_step = 2000
    print_step = 100000000

    # tf Graph input
    x = tf.placeholder("float", [None, n_input, 1])
    y = tf.placeholder("float", [None, vocab_size])

    # RNN output node weights and biases
    weights = {
        'out': tf.Variable(tf.random_normal([n_hidden, vocab_size]))
    }
    biases = {
        'out': tf.Variable(tf.random_normal([vocab_size]))
    }

    pred = RNN(x, weights, biases)

    # Loss and optimizer
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=pred, labels=y))
    optimizer = tf.train.RMSPropOptimizer(learning_rate=learning_rate).minimize(cost)

    # Model evaluation
    correct_pred = tf.equal(tf.argmax(pred, 1), tf.argmax(y, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

    # Initializing the variables
    init = tf.global_variables_initializer()

    saver = tf.train.Saver()
    # Launch the graph
    with tf.Session() as session:
        session.run(init)
        step = 0
        offset = random.randint(0, len(train_input_dialog_vectors))
        end_offset = n_input + 1
        acc_total = 0
        loss_total = 0

        writer.add_graph(session.graph)

        while step < training_iters:
            if (step + 1) % print_step == 0:
                print("STEP: " + str(step))
            # Generate a minibatch. Add some randomness on selection process.
            if offset > (len(train_input_dialog_vectors) - end_offset):
                offset = random.randint(0, n_input + 1)

            symbols_in_keys = train_input_dialog_vectors[offset]  # for i in range(offset, offset + n_input)]
            symbols_in_keys = numpy.reshape(numpy.array(symbols_in_keys), [-1, n_input, 1])

            symbols_out_onehot = numpy.zeros([vocab_size], dtype=float)
            symbols_out_onehot[train_output_dialog_vectors[offset][0]] = 1.0
            symbols_out_onehot = numpy.reshape(symbols_out_onehot, [1, -1])

            _, acc, loss, onehot_pred = session.run([optimizer, accuracy, cost, pred],
                                                    feed_dict={x: symbols_in_keys, y: symbols_out_onehot})
            loss_total += loss
            acc_total += acc
            if (step + 1) % display_step == 0:
                saver.save(session, "weights/model_" + mode + "_" + str(step) + ".ckpt")
                print("Iter= " + str(step + 1) + ", Average Loss= " +
                      "{:.6f}".format(loss_total / display_step) + ", Average Accuracy= " +
                      "{:.2f}%".format(100 * acc_total / display_step))
                acc_total = 0
                loss_total = 0
                if tf_idf:
                    symbols_in = train_input_dialog_vectors[offset]
                else:
                    symbols_in = [dictionary[train_input_dialog_vectors[offset][i]] for i in range(len(train_input_dialog_vectors[offset]))]  # [training_data[i] for i in range(offset, offset + n_input)]
                symbols_out = [dictionary[train_output_dialog_vectors[offset][i]] for i in range(len(train_output_dialog_vectors[offset]))]  # training_data[offset + n_input]
                symbols_out_pred = dictionary[int(tf.argmax(onehot_pred, 1).eval())]
                print("%s - [%s] vs [%s]" % (symbols_in, symbols_out, symbols_out_pred))
            step += 1
            offset += (n_input + 1)
        print("Optimization Finished!")
        print("Elapsed time: ", elapsed(time.time() - start_time))
        print("Run on command line.")
        print("\ttensorboard --logdir=%s" % (logs_path))
        if to_show_in_browser:
            print("Point your web browser to: http://localhost:6006/")
            while True:
                prompt = "%s words: " % n_input
                sentence = input(prompt)
                sentence = sentence.strip()
                words = sentence.split(' ')
                if len(words) != n_input:
                    continue
                try:
                    symbols_in_keys = [reverse_dictionary[str(words[i])] for i in range(len(words))]
                    for i in range(32):
                        keys = numpy.reshape(numpy.array(symbols_in_keys), [-1, n_input, 1])
                        onehot_pred = session.run(pred, feed_dict={x: keys})
                        onehot_pred_index = int(tf.argmax(onehot_pred, 1).eval())
                        sentence = "%s %s" % (sentence, dictionary[onehot_pred_index])
                        symbols_in_keys = symbols_in_keys[1:]
                        symbols_in_keys.append(onehot_pred_index)
                    print(sentence)
                except:
                    print("Word not in dictionary")


if __name__ == '__main__':
    get_input()
    dictionary, reverse_dictionary, _ = init_dicts()
    if tf_idf:
        convert_input_to_tfidf()
    else:
        convert_input_to_vectors()
    # # for index, vector in enumerate(train_input_dialog_vectors):
    # #     train_input_dialog_vectors[index] = [int((value + 5) * 1000) for value in vector]
    # # for index, vector in enumerate(train_output_dialog_vectors):
    # #     train_output_dialog_vectors[index] = [int((value + 5) * 1000) for value in vector]
    #
    # init_model()
    # # dataset = tf.data.Dataset.from_tensor_slices(
    # #     (numpy.asarray(train_input_dialog_vectors), numpy.asarray(train_output_dialog_vectors))).shuffle(BUFFER_SIZE)
    # # dataset = dataset.apply(tf.contrib.data.batch_and_drop_remainder(BATCH_SIZE))
    # train_model()
    # predict_with_model()
    main()

