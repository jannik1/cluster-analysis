import numpy as np
import pandas
import json
import datetime
import logging

from sklearn.mixture import GaussianMixture

import src.data_statistics as data_statistics
import src.my_clustering as my_clustering
import re

from src.word_vector_analysis import visualize_clustering_results
from scipy.spatial.distance import cdist


class MyFormatter(logging.Formatter):
    converter = datetime.datetime.fromtimestamp

    def formatTime(self, record, datefmt=None):
        ct = self.converter(record.created)
        if datefmt:
            s = ct.strftime(datefmt)
        else:
            t = ct.strftime("%Y-%m-%d %H:%M:%S")
            s = "%s,%03d" % (t, record.msecs)
        return s


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

console = logging.StreamHandler()
logger.addHandler(console)

formatter = MyFormatter(fmt='%(asctime)s %(message)s', datefmt='%Y-%m-%d,%H:%M:%S.%f')
console.setFormatter(formatter)

ubuntu_index = 2
use_gaussian = False


def get_cluster_centers():
    cluster_center_data = data_statistics.get_cluster_file()
    cluster_center_data = [cluster_center.split(' ') for cluster_center in cluster_center_data['10']]
    local_cluster_centers = []
    for cluster_center in cluster_center_data:
        try:
            cluster_center_x = float(re.sub(r"(\[\])+", "", cluster_center[0][1:]))
            try:
                cluster_center_y = float(re.sub(r"(\[\])+", "", cluster_center[1][:len(cluster_center[1]) - 1]))
            except ValueError:
                cluster_center_y = float(re.sub(r"(\[\])+", "", cluster_center[2][:len(cluster_center[1]) - 1]))
        except ValueError:
            cluster_center_x = float(re.sub(r"(\[\])+", "", cluster_center[1][1:]))
            cluster_center_y = float(re.sub(r"(\[\])+", "", cluster_center[2][:len(cluster_center[1]) - 1]))
        local_cluster_centers.append((cluster_center_x, cluster_center_y))
    return local_cluster_centers


def calc_distance(cluster_center_coords, count_coords):
    return cdist(np.array([cluster_center_coords]), np.array([count_coords]), 'canberra')[0][0]  # np.sqrt(np.power(abs(count_coords[0] - cluster_center_coords[0]), 2)
           #         + np.power(abs(count_coords[1] - cluster_center_coords[1]), 2))


def get_best_cluster(count, count_index, set_values=True):
    global csv_data
    min_index = 0
    min_dist = 100000
    for index, cluster_center in enumerate(cluster_centers):
        dist = calc_distance(cluster_centers[index], count)
        if dist < min_dist:
            min_index = index
            min_dist = dist
    if set_values:
        # csv_data.set_value(count_index, 'actual_cluster', true_labels[count_index])
        csv_data.set_value(count_index, 'x', count[0])
        csv_data.set_value(count_index, 'y', count[1])
        csv_data.set_value(count_index, 'distance', min_dist)
    # if min_index == 0:
    #     min_index = 1
    # elif min_index == 1:
    #     min_index = 0
    # elif min_index == 2:
    #     min_index = 3
    # elif min_index == 3:
    #     min_index = 2
    return min_index


if __name__ == '__main__':
    logger.debug('INIT TIME')
    date = "20-09-2018"  # "18-09-2018"
    time = "16-41"  # "16-00"
    results_dir = "results-server"
    best = "BEST - "
    cluster_file_name = "kvret_train_public - Attributes of 4 Clusters with tf-idf - " + date + " " + time + ".csv"
    cluster_dir_name = best + "kvret_train_public with tf-idf - " + date + " " + time + "/"
    cluster_file = pandas.read_csv("../../" + results_dir + "/" + cluster_dir_name + cluster_file_name, encoding="ISO-8859-1",
                                   sep=';')
    data_statistics.set_cluster_file(cluster_file)
    cluster_centers = get_cluster_centers()
    train_file_name = "kvret_train_public - 4 Clusters with tf-idf - " + date + " " + time + ".csv"
    train_file_name = "../../" + results_dir + "/" + cluster_dir_name + train_file_name
    ubuntu_data = pandas.DataFrame(my_clustering.clean_words((my_clustering.read_from_csv("test-new-converted-min", True))),
                                   columns=["clean_sum", "sum", "Full_Context"])
    file_name = "kvret_test_public"
    with open("../../data/" + file_name + '.json') as json_file:
        json_data = json.load(json_file)
    print("START TO CLEAN WORDS")
    my_clustering.set_true_labels([ubuntu_index] * 100)
    csv_data = my_clustering.clean_words_json(json_data, local_file_name=file_name, only_first_sentence=True)
    csv_data = pandas.concat([ubuntu_data, csv_data], axis=0, ignore_index=True, sort=False)
    loaded_vectorizer = my_clustering.load_vectorizer(cluster_dir_name, results_dir)
    loaded_pca = my_clustering.load_pca(cluster_dir_name, results_dir)
    csv_data['cluster_num'] = pandas.DataFrame([0] * len(csv_data))
    csv_data['actual_cluster'] = pandas.Series()
    true_labels = my_clustering.get_true_labels()
    only_test_dialogs = 404
    chunk_size = 1
    nearest_dialogs = [int(re.sub("Nearest neighbor index: ", "", dialog_index)) for dialog_index in cluster_file['11']]
    # my_clustering.set_nearest_dialogs(nearest_dialogs)
    nearest_dialogs_cluster_nums = []
    logger.debug('INIT LOOP TIME')
    for i in range(0, len(csv_data), chunk_size):
        clustering_data = csv_data[i:i + chunk_size]  # csv_data.drop(csv_data.index[range(1, len(csv_data))])
        counts = my_clustering.vectorize_words("tf-idf", True, clustering_data, loaded_vectorizer)
        if i == 0:
            reduced_counts = loaded_pca.fit_transform(counts.todense())
        else:
            reduced_counts = loaded_pca.transform(counts.todense())
        my_clustering.set_reduced_counts(reduced_counts)
        # local_counts = loaded_vectorizer.transform(csv_data[i*chunk_size:(i+1)*chunk_size])
        # total_reduced_counts = counts.todense().tolist()
        # total_reduced_counts.append(local_counts.todense().tolist()[0])
        # total_reduced_counts = loaded_pca.fit_transform(total_reduced_counts)
        # reduced_counts = total_reduced_counts
        # my_clustering.set_reduced_counts(reduced_counts)
        # reduced_counts = reduced_counts[len(reduced_counts) - chunk_size:len(reduced_counts)]
        if not use_gaussian:
            # cluster_centers = reduced_counts[0:4]
            for k in range(len(reduced_counts) - chunk_size, len(reduced_counts)):  # chunk_size):
                best_cluster = get_best_cluster(reduced_counts[k], i + (k - (len(reduced_counts) - chunk_size)))
                print("PREDICTED: " + str(best_cluster))
                csv_data.set_value(i + (k - (len(reduced_counts) - chunk_size)), 'cluster_num', best_cluster)
                print(str(k))
            if i == 0:
                for k in range(4):
                    best_cluster = get_best_cluster(reduced_counts[nearest_dialogs[k]], -1, set_values=False)
                    print("PREDICTED: " + str(best_cluster))
                    nearest_dialogs_cluster_nums.append(best_cluster)
                    print(str(k))
        else:
            my_clustering.clustering_alg = GaussianMixture(n_components=30, n_init=10)
            my_clustering.clustering_alg_name = "gaussian"
            cluster_nums = my_clustering.do_clustering(4, True)
            reduced_counts_plus_centers = np.append(reduced_counts, my_clustering.clustering_alg.means_, axis=0)
            my_clustering.reduced_counts_plus_centers = reduced_counts_plus_centers
            print("SOME UBUNTU VALUES:")
            for num in range(5):
                print(str(cluster_nums[num]))
            print("END OF SOME UBUNTU VALUES!")
            # for k in range(4, len(cluster_nums)):
            #     csv_data.set_value(i + k - 4, 'cluster_num', cluster_nums[k])
            print("CLUSTER VALUES: ")
            for k in range(i, i + chunk_size):
                print(cluster_nums[-1])
                # csv_data.set_value(k, 'cluster_num', cluster_nums[-1])
            for l in range(4):
                print(cluster_nums[l])
            # TODO: Fix task indexes in every loop
            my_clustering.set_cluster_nums(cluster_nums, use_gaussian)
            my_clustering.set_true_labels(cluster_nums)
            my_clustering.fix_task_indexes(cluster_centers, train_file_name)
            predicted_labels = my_clustering.get_true_labels()
            print("FIXED? TASK INDEX NOW: " + str(predicted_labels[len(predicted_labels) - 1]))
            for k in range(i, i + chunk_size):
                csv_data.set_value(k, 'cluster_num', predicted_labels[len(predicted_labels) - 1])
        print(str(i))
        reduced_counts = reduced_counts[len(reduced_counts) - chunk_size:len(reduced_counts)]
        logger.debug('END LOOP ' + str(i))
    print("LOOP DONE")
    logger.debug('FINAL END LOOP')
    # for k in range(0, 4):
    #     csv_data.set_value(k, 'cluster_num', k)
    print("TIME SAVED")
    nearest_dialogs_cluster_nums.extend(csv_data['cluster_num'])
    print("EXTENDED NEAREST DIALOGS")
    my_clustering.set_cluster_nums(nearest_dialogs_cluster_nums, use_gaussian)
    print("SET CLUSTER NUMS")
    #my_clustering.set_true_labels(true_labels)
    #my_clustering.fix_task_indexes(cluster_centers, train_file_name)
    #true_labels = my_clustering.get_true_labels()
    csv_data['actual_cluster'] = pandas.Series(true_labels)
    if not use_gaussian:
        my_clustering.set_true_labels(csv_data['cluster_num'])
        print("SET TRUE LABELS")
        my_clustering.fix_task_indexes(cluster_centers, train_file_name)
        print("FIXED TASK INDEXES")
        predicted_labels = my_clustering.get_true_labels()
    else:
        predicted_labels = csv_data['cluster_num']
    logger.debug('FIXED CLUSTER NUMS')
    print(str(len(csv_data)))
    print(str(len(predicted_labels)))
    relevant_cluster_nums = [predicted_labels[i] for i in range(len(csv_data) - only_test_dialogs, len(csv_data))]
    csv_data['cluster_num'] = pandas.Series(relevant_cluster_nums)
    # save cluster_nums and evaluate
    try:
        read_in_labels = []
        for i in range(0, only_test_dialogs):
            read_in_labels.append(csv_data['cluster_num'][i])
        print(str(len(read_in_labels)))
        print(str(len(true_labels)))
        data_statistics.set_true_labels(true_labels)
        print(str(data_statistics.get_accuracy_score(read_in_labels)))
    except ValueError:
        print("ValueError")
    my_clustering.write_source_to_csv(csv_data, file_name + " - Routing bot - 4 Clusters with tf-idf",
                                      "../../" + results_dir + "/" + cluster_dir_name)
    visualize_clustering_results.set_dialog_colors(true_labels)
    visualize_clustering_results.plot(reduced_counts, "reality - ")
    visualize_clustering_results.set_dialog_colors(read_in_labels)
    visualize_clustering_results.plot(reduced_counts, "prediction - ")
