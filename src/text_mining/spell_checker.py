import json
import re
import pandas
import enchant

from nltk import word_tokenize
from src import data_statistics, my_clustering


def is_number(word):
    return re.match("[0-9]", word)


def is_special_char(word):
    return re.match('[\t,.:;_=+*~^\-"!?@%&#$`/|\'()[\]{}]', word)


kb_vocab_file_name = "../../../data/kvret_entities_cleaning.json"
kb_vocabulary = {}
loaded_kb = False


def load_kb():
    global kb_vocabulary
    global loaded_kb
    with open(kb_vocab_file_name, 'r', encoding='utf-8') as f:
        loaded_file = json.load(f)
    for tag in loaded_file:
        print(tag)
        if tag == "poi":
            for poi in loaded_file[tag]:
                kb_vocabulary[poi["address"]] = "poi_address"
                kb_vocabulary[poi["poi"]] = "poi_poi"
                kb_vocabulary[poi["type"]] = "poi_type"
        else:
            for real_tag in loaded_file[tag]:
                kb_vocabulary[real_tag] = tag
    loaded_kb = True


def is_in_kb(word, word_index):
    global loaded_kb
    if not loaded_kb:
        load_kb()
    return kb_vocabulary.__contains__(word)\
        or (word_index + 1 < len(tokenized) and kb_vocabulary.__contains__(tokenized[word_index] + " " + tokenized[word_index + 1]))\
        or (word_index + 2 < len(tokenized) and kb_vocabulary.__contains__(tokenized[word_index] + " " + tokenized[word_index + 1] + " " + tokenized[word_index + 2]))\
        or (word_index + 3 < len(tokenized) and kb_vocabulary.__contains__(tokenized[word_index] + " " + tokenized[word_index + 1] + " " + tokenized[word_index + 2] + " " + tokenized[word_index + 3]))\
        or (word_index - 1 >= 0 and kb_vocabulary.__contains__(tokenized[word_index - 1] + " " + tokenized[word_index]))\
        or (word_index - 1 >= 0 and word_index + 1 < len(tokenized) and kb_vocabulary.__contains__(tokenized[word_index - 1] + " " + tokenized[word_index] + " " + tokenized[word_index + 1]))\
        or (word_index - 1 >= 0 and word_index + 2 < len(tokenized) and kb_vocabulary.__contains__(tokenized[word_index - 1] + " " + tokenized[word_index] + " " + tokenized[word_index + 1] + " " + tokenized[word_index + 2]))\
        or (word_index - 2 >= 0 and kb_vocabulary.__contains__(tokenized[word_index - 2] + " " + tokenized[word_index - 1] + " " + tokenized[word_index]))\
        or (word_index - 2 >= 0 and word_index + 1 < len(tokenized) and kb_vocabulary.__contains__(tokenized[word_index - 2] + " " + tokenized[word_index - 1] + " " + tokenized[word_index] + " " + tokenized[word_index + 1]))\
        or (word_index - 3 >= 0 and kb_vocabulary.__contains__(tokenized[word_index - 3] + " " + tokenized[word_index - 2] + " " + tokenized[word_index - 1] + " " + tokenized[word_index]))


correct_words = ["ubuntu", "dvds", "hdd", "mozilla", "thunderbird", "martha", "cafe", "york", "chinese", "san", "francisco",
                 "alex", "barista", "starbucks", "jon", "mateo", "ana", "forecasted", "los", "angeles", "marie", "i.e",
                 "ok", "nvidia", "iso", "geforce", "gtx", "wifi", "menlo", "__eou__", "__eot__", "eou", "eot"]


def is_correct(word):
    return word in correct_words


if __name__ == '__main__':
    mode = "train"
    ubuntu_data = pandas.DataFrame(
        my_clustering.clean_words((my_clustering.read_from_csv(mode + "-new-converted", False))),
        columns=["clean_sum", "sum", "Label", "Utterance"])
    file_name = "kvret_" + mode + "_public"
    with open("../../../data/" + file_name + '.json') as json_file:
        json_data = json.load(json_file)
    print("START TO CLEAN WORDS")
    csv_data = data_statistics.clean_words_json(json_data, file_name, False)
    csv_data = pandas.concat([ubuntu_data, csv_data], axis=0, ignore_index=True, sort=False)
    d = enchant.Dict("en_UK")   # create dictionary for US English
    correct_ubuntu = 0
    correct_rest = 0
    wrong_ubuntu = 0
    wrong_rest = 0
    wrong_words_ubuntu = []
    wrong_words_rest = []
    longest_ubuntu = 0
    shortest_ubuntu = 1000
    ubuntu_dict = {}
    longest_rest = 0
    shortest_rest = 1000
    rest_dict = {}
    for line_index, line in enumerate(csv_data['sum']):
        line = re.sub("gonna", "going to", line)
        if line_index < len(ubuntu_data['sum']):  # line_index < 15000:
            if csv_data["Label"][line_index] == 1:
                line += csv_data['Utterance'][line_index]
            tokenized = word_tokenize(line)
            if longest_ubuntu < len(tokenized):
                longest_ubuntu = len(tokenized)
            if shortest_ubuntu > len(tokenized):
                shortest_ubuntu = len(tokenized)
            for word_index, word in enumerate(tokenized):
                if ubuntu_dict.get(word) is None:
                    ubuntu_dict[word] = 0
        else:
            tokenized = word_tokenize(line)
            if longest_rest < len(tokenized):
                longest_rest = len(tokenized)
            if shortest_rest > len(tokenized):
                shortest_rest = len(tokenized)
            for word_index, word in enumerate(tokenized):
                if rest_dict.get(word) is None:
                    rest_dict[word] = 0
        # for word_index, word in enumerate(tokenized):
        #     if not is_special_char(word) and not is_number(word):
        #         if d.check(word) or is_in_kb(word, word_index) or is_correct(word) or d.check(word.lower())\
        #                 or is_in_kb(word.lower(), word_index)\
        #                 or is_correct(word.lower()) or d.check(word.title()) or is_in_kb(word.title(), word_index)\
        #                 or is_correct(word.title()):
        #             if line_index < 800:
        #                 correct_ubuntu += 1
        #             else:
        #                 correct_rest += 1
        #         else:
        #             if line_index < 800:
        #                 wrong_ubuntu += 1
        #                 wrong_words_ubuntu.append(word)
        #             else:
        #                 wrong_rest += 1
        #                 wrong_words_rest.append(word)
        #             print(line)
    print("REST DICT: " + str(len(rest_dict)))
    print("LONGEST REST: " + str(longest_rest))
    print("SHORTEST REST: " + str(shortest_rest))
    print("UBUNTU DICT: " + str(len(ubuntu_dict)))
    print("LONGEST UBUNTU: " + str(longest_ubuntu))
    print("SHORTEST UBUNTU: " + str(shortest_ubuntu))
    print("NUMBER OF LINES: " + str(len(csv_data['sum'])))
    print("CORRECT WORDS UBUNTU: " + str(correct_ubuntu))
    print("WRONG WORDS UBUNTU: " + str(wrong_ubuntu))
    print("CORRECT UBUNTU: " + str(correct_ubuntu / (correct_ubuntu + wrong_ubuntu)))
    print("CORRECT WORDS REST: " + str(correct_rest))
    print("WRONG WORDS REST: " + str(wrong_rest))
    print("CORRECT REST: " + str(correct_rest / (correct_rest + wrong_rest)))
    df = pandas.DataFrame(wrong_words_ubuntu)
    df.to_csv("WRONG_WORDS-" + mode + "-ubuntu.csv", sep=";")
    df = pandas.DataFrame(wrong_words_rest)
    df.to_csv("WRONG_WORDS-" + mode + "-rest.csv", sep=";")
