import datetime
import pandas

import spacy
from sklearn.decomposition import TruncatedSVD, PCA

now = datetime.datetime.now()
spacy_nlp = spacy.load('en')
train_dir_name = "../../../data/train_results"
train_file_name = "visualize_data"


def get_time():
    return now.strftime("%d-%m-%Y %H-%M")


def get_input():
    global train_data
    global train_dialogs_input
    train_data = pandas.read_csv(train_dir_name + "/" + train_file_name + ".csv", encoding="ISO-8859-1", sep=';')
    train_data.fillna("")
    train_dialogs_input = train_data['clean_sum']  # ['input']


def compute_vectors():
    global vectors
    vectors = []
    analyzed_sentences = spacy_nlp(' '.join(sample_sentences))
    for index, sentence in enumerate(sample_sentences):
        # analyzed_sentence = spacy_nlp(sentence)
        vectors.append(analyzed_sentences[index].vector)


def reduce_vectors():
    global reduced_vectors
    svd = PCA(n_components=2)
    reduced_vectors = svd.fit_transform(vectors)


def plot():
    from matplotlib import pyplot as plt
    from itertools import cycle

    plt.figure(1)
    plt.clf()

    colors = cycle('bgrcmykbgrcmykbgrcmykbgrcmyk')
    for k, col in zip(range(4), colors):
        my_members = pandas.Series(dialog_colors) == k
        plt.plot(reduced_vectors[my_members, 0], reduced_vectors[my_members, 1], col + '.', markersize=14)
        for index, member in enumerate(my_members):
            if member:
                plt.text(reduced_vectors[index, 0], reduced_vectors[index, 1], sample_sentences[index])
        # plt.plot(cluster_center[0], cluster_center[1], 'o', markerfacecolor=col,
        #          markeredgecolor='k', markersize=14)
    plt.title('')
    plt.savefig("sample sentences - alternative plot - " + get_time() + ".pdf")
    plt.show()


def fill_sample_sentences():
    no_dialogs = 5
    offset = int((3200 - 900) / no_dialogs)
    for cluster_no in range(0, 3):
        for i in range(1, no_dialogs):
            local_offset = 800
            while train_data['actual_cluster'][i*offset + local_offset] != cluster_no:
                local_offset += 1
            # sample_sentences.append(input("Please enter your sentence.\n"))
            sample_sentences.append(train_dialogs_input[i * offset + local_offset])
            print(train_dialogs_input[i * offset + local_offset])
            # colors.append(input("Please specify the color. (R)ed = Schedule, (B)lue = Weather, (Y)ellow = Navigation,"
            #                     "(G)reen = Ubuntu"))
            dialog_colors.append(train_data['actual_cluster'][i*offset + local_offset])
            print(str(train_data['actual_cluster'][i*offset + local_offset]))
    for i in range(1, no_dialogs):
        local_offset = 0
        while train_data['actual_cluster'][i + local_offset] != 3:
            local_offset += 1
        # sample_sentences.append(input("Please enter your sentence.\n"))
        sample_sentences.append(train_dialogs_input[i + local_offset])
        print(train_dialogs_input[i + local_offset])
        # colors.append(input("Please specify the color. (R)ed = Schedule, (B)lue = Weather, (Y)ellow = Navigation,"
        #                     "(G)reen = Ubuntu"))
        dialog_colors.append(train_data['actual_cluster'][i + local_offset])
        print(str(train_data['actual_cluster'][i + local_offset]))


if __name__ == '__main__':
    sample_sentences = []
    dialog_colors = []
    get_input()
    fill_sample_sentences()
    # sample_sentences = ["Navigate to gas station", "Navigate to super market", "Weather for tomorrow in New York", "Weather for tomorrow in Dortmund",
    #                     "Weather for today in Ottawa", "Schedule doctor appointment for Friday", "Schedule date with Lisa for Sunday",
    #                     "How can I change my login name in Ubuntu", "Where can I change login picture for Ubuntu"]
    # dialog_colors = [0, 0, 1, 1, 1, 2, 2, 3, 3]
    # sample_sentences = ["Where is the nearest doctor", "Navigate to super market", "How will the temperature be tomorrow in New York", "Weather for tomorrow in Dortmund",
    #                     "When is my next doctor appointment", "Schedule date with Lisa for Sunday",
    #                     "How can I install a weather app", "Where can I change login picture for Ubuntu"]
    # dialog_colors = [0, 0, 1, 1, 2, 2, 3, 3]
    # sample_sentences = ["nearest doctor", "Navigate super market", "temperature tomorrow New York", "Weather tomorrow Dortmund",
    #                     "next doctor appointment", "Schedule date Lisa Sunday",
    #                     "install weather app", "change login picture Ubuntu"]
    # dialog_colors = ([0, 0, 1, 1, 2, 2, 3, 3])
    sample_sentences = (["king", "queen", "man", "woman", "prince", "princess", "girl", "boy", "actor", "actress", "lady", "sir"])
    dialog_colors = ([3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3])
    sample_sentences.extend(["Germany", "Berlin", "England", "London", "France", "Paris", "Spain", "Madrid", "Italy", "Rome", "Russia", "Moscow"])
    dialog_colors.extend([3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2])
    # for index, color in enumerate(colors):
        # if color[0].lower() == "r":
        #     colors[index] = 0
        # elif color[0].lower() == "b":
        #     colors[index] = 1
        # elif color[0].lower() == "y":
        #     colors[index] = 2
        # elif color[0].lower() == "g":
        #     colors[index] = 3
        # else:
        #     print("Unknown color detected!")

    compute_vectors()
    reduce_vectors()
    plot()
