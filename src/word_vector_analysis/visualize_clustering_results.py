import datetime
import pandas
import platform
import matplotlib

# import spacy
# from sklearn.decomposition import TruncatedSVD, PCA

now = datetime.datetime.now()
# spacy_nlp = spacy.load('en')
train_dir_name = "../../../data/train_results"
train_file_name = "kvret_test_public - Routing bot - 4 Clusters with tf-idf - 17-09-2018 11-45"


def get_time():
    return now.strftime("%d-%m-%Y %H-%M")


def get_input():
    global csv_data
    global train_dialogs_input
    global sample_sentences
    csv_data = pandas.read_csv(train_dir_name + "/" + train_file_name + ".csv", encoding="ISO-8859-1", sep=';')
    csv_data.fillna("")
    sample_sentences = csv_data['clean_sum']  # ['input']


def set_dialog_colors(local_cols):
    global dialog_colors
    dialog_colors = local_cols


def plot(local_reduced_vectors=None, file_name_appendix=""):
    if platform.node() != 'HP-Laptop':
        print('no display found. Using non-interactive Agg backend')
        matplotlib.use('Agg')
    else:
        matplotlib.use('TkAgg')
    global reduced_vectors
    from matplotlib import pyplot as plt
    from itertools import cycle

    if local_reduced_vectors is not None:
        reduced_vectors = local_reduced_vectors

    plt.figure(1)
    plt.clf()

    colors = cycle('bgrcmykbgrcmykbgrcmykbgrcmyk')
    for k, col in zip(range(4), colors):
        my_members = pandas.Series(dialog_colors) == k
        plt.plot([reduced_vectors[index][0] for index, vec in enumerate(reduced_vectors) if my_members[index]],
                 [reduced_vectors[index][1] for index, vec in enumerate(reduced_vectors) if my_members[index]],
                 col + '.', markersize=8)
        # plt.plot(reduced_vectors[my_members][0], reduced_vectors[my_members][1], col + '.', markersize=14)
        # for index, member in enumerate(my_members):
        #     if member:
        #         plt.text(reduced_vectors[index][0], reduced_vectors[index][1], sample_sentences[index])
        # plt.plot(cluster_center[0], cluster_center[1], 'o', markerfacecolor=col,
        #          markeredgecolor='k', markersize=14)
    plt.title('')
    plt.savefig("sample sentences - alternative plot - " + file_name_appendix + get_time() + ".pdf")
    plt.show()


if __name__ == '__main__':
    get_input()
    dialog_colors = csv_data['actual_cluster']  # ([3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3])
    dialog_colors = [int(str(col)[0:1]) for col in dialog_colors]

    reduced_vectors = [(csv_data.get_value(i, 'x'), csv_data.get_value(i, 'y')) for i in range(len(csv_data))]
    plot()
