import numpy

import pandas

import tensorflow as tf

from src.routing_bot_nn import init_dicts, mode, RNN, get_input, get_train_input_dialog_vectors, \
    get_train_output_dialog_vectors, convert_input_to_vectors, get_test_input, convert_test_input_to_vectors, \
    get_test_input_dialog_vectors, get_test_output_dialog_vectors


def run_example(local_g_plot):
    global vocabulary
    try:
        vocabulary
    except NameError:
        vocabulary = get_vocab()
    sentence = 20 * [0]
    inputs_int = get_inputs_int()
    sentences = len(inputs_int) * [20 * [0]]
    # sentence_numbers = 20 * [0]
    for dialog_pos, sentence[0], sentence[1], sentence[2], sentence[3], sentence[4], sentence[5],\
        sentence[6], sentence[7], sentence[8], sentence[9], sentence[10], sentence[11], sentence[12], sentence[13],\
        sentence[14], sentence[15], sentence[16], sentence[17], sentence[18], sentence[19] in local_g_plot:
        if 0 <= int(dialog_pos.__round__()) < len(inputs_int):
            in_span = True
            for word_index, word in enumerate(sentence):
                if not (0 <= int(word.__round__()) < vocabulary.size()):
                    sentence[word_index] = 0
            if in_span:
                sentences[int(dialog_pos.__round__())] = list(map(int, list(map(round, sentence))))  # +=
        # sentence_numbers[int(word_pos.__round__())] += 1
    # for word_index, word in enumerate(sentence):
    #     sentence[word_index] = int((sentence[word_index] / sentence_numbers[word_index]).__float__())
    dialogues = []
    for dialogue in sentences:
        dialogue = ' '.join(vocabulary.int_to_string(dialogue))
        dialogues.append(dialogue)
    return dialogues


if __name__ == "__main__":
    # Parameters
    learning_rate = 0.001
    training_iters = 50000
    display_step = 1000
    print_step = 10
    n_input = 30
    # number of units in RNN cell
    n_hidden = 512

    get_input()
    get_test_input()
    dictionary, reverse_dictionary, _ = init_dicts()

    vocab_size = len(dictionary)

    # tf Graph input
    x = tf.placeholder("float", [None, n_input, 1])
    y = tf.placeholder("float", [None, vocab_size])

    # RNN output node weights and biases
    weights = {
        'out': tf.Variable(tf.random_normal([n_hidden, vocab_size]))
    }
    biases = {
        'out': tf.Variable(tf.random_normal([vocab_size]))
    }

    pred = RNN(x, weights, biases)

    # Loss and optimizer
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=pred, labels=y))
    optimizer = tf.train.RMSPropOptimizer(learning_rate=learning_rate).minimize(cost)

    # Model evaluation
    correct_pred = tf.equal(tf.argmax(pred, 1), tf.argmax(y, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

    # Initializing the variables
    init = tf.global_variables_initializer()

    saver = tf.train.Saver()
    session = tf.Session()
    i = 49999
    saver.restore(session, "weights/model_" + mode + "_" + str(i) + ".ckpt")
    session.run(init)

    input_data = pandas.Series(["Set reminder for football", "What is the weather today?", "How can I install ubuntu?",
                                "Where is the next gas station?"])
    output_data = pandas.Series(["0", "1", "2",
                                "3"])
    convert_test_input_to_vectors()
    test_input_dialog_vectors = get_test_input_dialog_vectors()
    test_output_dialog_vectors = get_test_output_dialog_vectors()
    correct = 0
    wrong = 0
    wrongly_zero = []
    wrongly_one = []
    wrongly_two = []
    wrongly_three = []
    wrong_preds = [wrongly_zero, wrongly_one, wrongly_two, wrongly_three]
    totally_wrong = []

    for index, input_dialogue in enumerate(test_input_dialog_vectors):
        symbols_in_keys = input_dialogue
        symbols_in_keys = numpy.reshape(numpy.array(symbols_in_keys), [-1, n_input, 1])

        symbols_out_onehot = numpy.zeros([vocab_size], dtype=float)
        symbols_out_onehot[test_output_dialog_vectors[index][0]] = 1.0
        symbols_out_onehot = numpy.reshape(symbols_out_onehot, [1, -1])

        _, acc, loss, onehot_pred = session.run([optimizer, accuracy, cost, pred],
                                                feed_dict={x: symbols_in_keys, y: symbols_out_onehot})

        symbols_in = [dictionary[input_dialogue[i]] for i in range(len(input_dialogue))]
        symbols_out = [dictionary[test_output_dialog_vectors[index][i]] for i in range(len(test_output_dialog_vectors[index]))]
        symbols_out_pred = dictionary[int(tf.argmax(onehot_pred, 1).eval(session=session))]
        # print("%s - [%s] vs [%s]" % (symbols_in, symbols_out, symbols_out_pred))
        if index % 10 == 0 and index > 0:
            pred_accuracy = correct / (wrong + correct)
            print("STEP " + str(index) + ": " + str(pred_accuracy))
        if str(symbols_out[0]) == str(symbols_out_pred):
            correct += 1
        else:
            wrong += 1
            try:
                wrong_preds[int(symbols_out_pred)].append(int(symbols_out[0]))
            except:
                totally_wrong.append(symbols_out_pred)
    pred_accuracy = correct / (wrong + correct)
    print(str(pred_accuracy))
    for index, wrong_list in enumerate(wrong_preds):
        print("FALSELY IDENTIFIED AS " + str(index) + ": " + str(len(wrong_list)))
        true_indexes = [0, 0, 0, 0]
        for wrong_index in wrong_list:
            true_indexes[wrong_index] += 1
        print("DISTRIBUTION: " + str(true_indexes[0]) + ", " + str(true_indexes[1]) + ", " + str(true_indexes[2]) + ", " + str(true_indexes[3]))
    for wrong_pred in totally_wrong:
        print(wrong_pred)
