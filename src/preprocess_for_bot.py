import re
import pandas
import src.clean_text as clean_text

only_cluster_nums = False
perfect_decomposition = True


def preprocess_and_save(dialog_type):
    converted_csv_data = pandas.DataFrame()
    inputs = pandas.Series()
    outputs = pandas.Series()
    error_count = 0
    cluster_nums = []
    actual_cluster_nums = []
    index_in_dialogs = []
    for dialog_index, dialog in enumerate(csv_data['sum']):
        if not decompositioned or (not perfect_decomposition and csv_data['cluster_num'][dialog_index] == dialog_type)\
                    or (perfect_decomposition and csv_data['actual_cluster'][dialog_index] == dialog_type) or only_cluster_nums:
            if not only_cluster_nums:
                utterances = [x.strip() for x in dialog.split("__eot__")]
                utterances = [re.sub("__eou__", "", utterance).strip() for utterance in utterances]
                for utterance_index, utterance in enumerate(utterances):
                    if utterance_index % 2 == 0:
                        dialog_subset = ""
                        temp_index = 0
                        while temp_index < utterance_index:
                            dialog_subset += " " + utterances[temp_index] + " "
                            temp_index += 1
                        dialog_subset += utterance
                        dialog_subset = dialog_subset.strip()
                        try:
                            output = utterances[utterance_index + 1]
                            if len(output.strip()) > 0 and len(dialog_subset) > 0:
                                outputs = outputs.append(pandas.Series(output))
                                inputs = inputs.append(pandas.Series(dialog_subset))
                                cluster_nums.append(csv_data['cluster_num'][dialog_index])
                                actual_cluster_nums.append(csv_data['actual_cluster'][dialog_index])
                                index_in_dialogs.append(csv_data['index_in_dialogs'][dialog_index])
                            else:
                                print("Empty dialog detected: " + dialog_subset + "\n Response: " + output)
                        except IndexError:
                            print("###ERROR###")
                            error_count += 1
            else:
                eot_index = dialog.find("__eot__")
                first_sentence = re.sub("__eou__", "", dialog[0:eot_index])
                first_sentence = clean_text.replace_with_tokens(first_sentence)
                inputs = inputs.append(pandas.Series(first_sentence))
                outputs.append(pandas.Series(csv_data['cluster_num'][dialog_index]))
                cluster_nums.append(csv_data['cluster_num'][dialog_index])
                actual_cluster_nums.append(csv_data['actual_cluster'][dialog_index])
                index_in_dialogs.append(csv_data['index_in_dialogs'][dialog_index])
    print(str(error_count))
    converted_csv_data['input'] = inputs
    converted_csv_data['output'] = outputs
    converted_csv_data['cluster_num'] = cluster_nums
    converted_csv_data['actual_cluster'] = actual_cluster_nums
    converted_csv_data['index_in_dialogs'] = index_in_dialogs
    if not only_cluster_nums:
        if not decompositioned:
            if perfect_decomposition:
                converted_csv_data.to_csv(dir_name + "/" + csv_file_name + " - perfect decomposition - preprocessedFinal.csv", sep=";", encoding="ISO-8859-1")
            else:
                converted_csv_data.to_csv(dir_name + "/" + csv_file_name + " - preprocessedFinal.csv", sep=";", encoding="ISO-8859-1")
        else:
            if perfect_decomposition:
                converted_csv_data.to_csv(dir_name + "/" + csv_file_name + " - " + str(dialog_type) + " - perfect decomposition - preprocessedFinal.csv", sep=";", encoding="ISO-8859-1")
            else:
                converted_csv_data.to_csv(dir_name + "/" + csv_file_name + " - " + str(dialog_type) + " - preprocessedFinal.csv", sep=";", encoding="ISO-8859-1")
    else:
        converted_csv_data.to_csv(dir_name + "/" + csv_file_name + " - cluster_nums - preprocessedFinal.csv", sep=";", encoding="ISO-8859-1")


if __name__ == '__main__':
    dataset_types = ["train", "test", "dev"]
    dates = ["20-09-2018", "24-09-2018", "24-09-2018"]
    times = ["16-41", "16-52", "16-42"]
    results_dirs = ["results-server", "results-server", "results-server"]
    bests = ["BEST - ", "BEST - ", "BEST - "]
    for index, _ in enumerate(dataset_types):
        dataset_type = dataset_types[index]
        date = dates[index]
        time = times[index]
        results_dir = results_dirs[index]
        best = bests[index]
        dir_name = "../../" + results_dir + "/" + best + "kvret_" + dataset_type + "_public with tf-idf - " + date + " " + time
        csv_file_name = "kvret_" + dataset_type + "_public - 4 Clusters with tf-idf - " + date + " " + time
        csv_data = pandas.read_csv(dir_name + "/" + csv_file_name + ".csv", delimiter=";", encoding="ISO-8859-1")
        if not only_cluster_nums:
            decompositioned = True
            for dialog_type in range(0, 4):
                print("preprocessing dialogs with type " + str(dialog_type))
                preprocess_and_save(dialog_type=dialog_type)
        decompositioned = False
        print("preprocessing all dialogs")
        preprocess_and_save(dialog_type=-1)
