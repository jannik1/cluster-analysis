# mostly from http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html#sphx-glr-auto-examples-model-selection-plot-confusion-matrix-py

import pandas

print(__doc__)

import itertools
import numpy as np
import matplotlib.pyplot as plt

from sklearn.metrics import confusion_matrix

date = "18-09-2018"
time = "16-00"
results_dir = "results-server"
best = "BEST - "
cluster_file_name = "kvret_train_public - 4 Clusters with tf-idf - " + date + " " + time + ".csv"
cluster_dir_name = best + "kvret_train_public with tf-idf - " + date + " " + time + "/"
csv_data = pandas.read_csv("../../" + results_dir + "/" + cluster_dir_name + cluster_file_name, encoding="ISO-8859-1",
                           sep=';')


def convert_to_int(list_of_str):
    list_of_int = []
    for text in list_of_str:
        list_of_int.append(int(str(text)[0:1]))
    return list_of_int


y_true = convert_to_int(csv_data['actual_cluster'])
y_pred = convert_to_int(csv_data['cluster_num'])


def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')


# Compute confusion matrix
cnf_matrix = confusion_matrix(y_true, y_pred)
np.set_printoptions(precision=2)

# Plot non-normalized confusion matrix
plt.figure()
plot_confusion_matrix(cnf_matrix, classes=['Ubuntu', 'Schedule', 'Weather', 'Navigate'],
                      title='Confusion matrix, without normalization')
plt.savefig("confusion-matrix.pdf")

# Plot normalized confusion matrix
plt.figure()
plot_confusion_matrix(cnf_matrix, classes=['Ubuntu', 'Schedule', 'Weather', 'Navigate'], normalize=True,
                      title='Normalized confusion matrix')
plt.savefig("normalized-confusion-matrix.pdf")
plt.show()
