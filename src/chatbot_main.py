from __future__ import unicode_literals
import os
import pickle
import re

import numpy
import time

import pandas
# import spacy
import tensorflow as tf


tf.enable_eager_execution()
# spacy_nlp = spacy.load('en')


from nltk import TextCollection, word_tokenize

train_dir_name = "../../data/train_results"
train_file_name = "train_data"
chunk_size = 100
vocab = {"<unk>": 0, "<tab>": 1, ".": 2, "?": 3, "!": 4, "<eos>": 5}


class Model(tf.keras.Model):
    def __init__(self, vocab_size, embedding_dim, units, batch_size):
        super(Model, self).__init__()
        self.units = units
        self.batch_sz = batch_size

        self.embedding = tf.keras.layers.Embedding(vocab_size, embedding_dim)

        if tf.test.is_gpu_available():
            self.gru = tf.keras.layers.CuDNNGRU(self.units,
                                                return_sequences=True,
                                                return_state=True,
                                                recurrent_initializer='glorot_uniform')
        else:
            self.gru = tf.keras.layers.GRU(self.units,
                                           return_sequences=True,
                                           return_state=True,
                                           recurrent_activation='sigmoid',
                                           recurrent_initializer='glorot_uniform')

        self.fc = tf.keras.layers.Dense(vocab_size)

    def call(self, x, hidden):
        x = self.embedding(x)

        # output shape == (batch_size, max_length, hidden_size)
        # states shape == (batch_size, hidden_size)

        # states variable to preserve the state of the model
        # this will be used to pass at every step to the model while training
        output, states = self.gru(x, initial_state=hidden)

        # reshaping the output so that we can pass it to the Dense layer
        # after reshaping the shape is (batch_size * max_length, hidden_size)
        output = tf.reshape(output, (-1, output.shape[2]))

        # The dense layer will output predictions for every time_steps(max_length)
        # output shape after the dense layer == (max_length * batch_size, vocab_size)
        x = self.fc(output)

        return x, states


# using sparse_softmax_cross_entropy so that we don't have to create one-hot vectors
def loss_function(real, preds):
    return tf.losses.sparse_softmax_cross_entropy(labels=real, logits=preds)


def get_input():
    global train_data
    global train_dialogs_input
    global train_dialogs_output
    train_data = pandas.read_csv(train_dir_name + "/" + train_file_name + ".csv", encoding="ISO-8859-1", sep=';')
    train_data.fillna("")
    train_dialogs_input = train_data['input']
    train_dialogs_output = train_data['output']


def get_vectors(dialogs):
    global vocab
    vectors = []
    for index, dialog in enumerate(dialogs):
        vector = []
        tokenized_dialog = word_tokenize(dialog)
        if len(tokenized_dialog) < 29:
            for i in range(29 - len(tokenized_dialog)):
                vector.append(vocab["<tab>"])
        for word_index, word in enumerate(tokenized_dialog):
            if word_index < 29:
                if not vocab.__contains__(word):
                    if len(vocab) >= vocab_size:
                        dialog = re.sub(re.escape(word), "<unk>", dialog)
                        word = "<unk>"
                    else:
                        vocab[word] = len(vocab)
                vector.append(vocab[word])
        if dialog[len(dialog) - 1] == "." or dialog[len(dialog) - 1] == "?"\
                or dialog[len(dialog) - 1] == "!":
            vector.append(vocab[dialog[len(dialog) - 1]])
        else:
            vector.append(vocab["<eos>"])
        # analysed_dialog = spacy_nlp(dialog)
        # vectors.append(analysed_dialog.vector)
        vectors.append(vector)
        if index % 100 == 0:
            print(str(index))
    return vectors


def convert_input_to_vectors():
    global train_input_dialog_vectors
    global train_output_dialog_vectors
    global my_dialogs
    global vocab
    if os.path.exists("input_vectors.pk") and os.path.exists("output_vectors.pk")\
            and os.path.exists("vocab.pk"):
        with open("input_vectors.pk", 'rb') as pickle_file:
            train_input_dialog_vectors = pickle.load(pickle_file)
        with open("output_vectors.pk", 'rb') as pickle_file:
            train_output_dialog_vectors = pickle.load(pickle_file)
        with open("vocab.pk", 'rb') as pickle_file:
            vocab = pickle.load(pickle_file)
    else:
        data_size = len(train_data)
        no_iterations = data_size // chunk_size
        train_input_dialog_vectors = []
        train_output_dialog_vectors = []
        train_inputs = [dialog if dialog == dialog else "NaN" for dialog in train_dialogs_input]
        train_outputs = [dialog if dialog == dialog else "NaN" for dialog in train_dialogs_output]
        train_inputs_plus_outputs = train_inputs + train_outputs
        my_dialogs = TextCollection(train_inputs_plus_outputs)
        # for i in range(no_iterations):
        train_input_dialog_vectors = get_vectors(train_inputs)
        with open('input_vectors.pk', 'wb') as fin:
            pickle.dump(train_input_dialog_vectors, fin)
        train_output_dialog_vectors = get_vectors(train_outputs)
        with open('output_vectors.pk', 'wb') as fin:
            pickle.dump(train_output_dialog_vectors, fin)
        with open('vocab.pk', 'wb') as fin:
            pickle.dump(vocab, fin)
        # train_input_dialog_vectors += get_vectors(
        #     train_dialogs_input[chunk_size * no_iterations:data_size])
        # train_output_dialog_vectors += get_vectors(
        #     train_dialogs_output[chunk_size * no_iterations:data_size])
    # for inputtt in train_input_dialog_vectors:
    #     print(str(inputtt))


def init_model():
    global model
    global optimizer
    model = Model(vocab_size, embedding_dim, units, BATCH_SIZE)
    optimizer = tf.train.AdamOptimizer()


def train_model():
    EPOCHS = 30

    for epoch in range(EPOCHS):
        start = time.time()

        # initializing the hidden state at the start of every epoch
        hidden = model.reset_states()

        for (batch, (inp, target)) in enumerate(dataset):
            with tf.GradientTape() as tape:
                # target = train_input_dialog_vectors[batch]
                # feeding the hidden state back into the model
                # This is the interesting step
                predictions, hidden = model(inp, hidden)

                # reshaping the target because that's how the
                # loss function expects it
                target = tf.reshape(target, (-1,))
                loss = loss_function(target, predictions)

            grads = tape.gradient(loss, model.variables)
            optimizer.apply_gradients(zip(grads, model.variables), global_step=tf.train.get_or_create_global_step())

            if batch % 100 == 0:
                print('Epoch {} Batch {} Loss {:.4f}'.format(epoch + 1,
                                                             batch,
                                                             loss))

        print('Epoch {} Loss {:.4f}'.format(epoch + 1, loss))
        print('Time taken for 1 epoch {} sec\n'.format(time.time() - start))


def get_char_from_vec(vector):
    best_similarity = 0
    best_word = ""
    for index, word, vocab_vector in enumerate(vocab.items()):
        if index % 100:
            print("Compared with " + str(index) + " word vectors")
        similarity = numpy.dot(vector, vocab_vector)
        if similarity > best_similarity:
            best_similarity = similarity
            best_word = word
    return best_word


def predict_with_model():
    # Evaluation step(generating text using the model learned)

    # number of characters to generate
    num_generate = 1000

    # You can change the start string to experiment
    start_string = 'Q'
    # converting our start string to numbers(vectorizing!)
    if not vocab.__contains__(start_string):
        vocab[start_string] = len(vocab)
    input_eval = [vocab[start_string]]
    input_eval = tf.expand_dims(input_eval, 0)

    # empty string to store our results
    text_generated = ''

    # low temperatures results in more predictable text.
    # higher temperatures results in more surprising text
    # experiment to find the best setting
    temperature = 1.0

    # hidden state shape == (batch_size, number of rnn units); here batch size == 1
    hidden = [tf.zeros((1, units))]
    for i in range(num_generate):
        predictions, hidden = model(input_eval, hidden)

        # using a multinomial distribution to predict the word returned by the model
        predictions = predictions / temperature
        predicted_id = tf.multinomial(tf.exp(predictions), num_samples=1)[0][0].numpy()

        # We pass the predicted word as the next input to the model
        # along with the previous hidden state
        input_eval = tf.expand_dims([predicted_id], 0)

        try:
            char_generated = next(key for key, value in vocab.items() if value == predicted_id)  # get_char_from_vec(predicted_id)
        except:
            char_generated = "<gen_unk>"
        text_generated += char_generated + " "

    text_generated = re.sub(r"\.", ".\n", text_generated)
    text_generated = re.sub(r"!", "!\n", text_generated)
    text_generated = re.sub(r"\?", "?\n", text_generated)
    text_generated = re.sub("<eos>", "<eos>\n", text_generated)
    print(start_string + text_generated)


if __name__ == '__main__':
    vocab_size = 10000
    embedding_dim = 30
    units = 1024
    BATCH_SIZE = 1
    BUFFER_SIZE = 100

    get_input()
    convert_input_to_vectors()
    # for index, vector in enumerate(train_input_dialog_vectors):
    #     train_input_dialog_vectors[index] = [int((value + 5) * 1000) for value in vector]
    # for index, vector in enumerate(train_output_dialog_vectors):
    #     train_output_dialog_vectors[index] = [int((value + 5) * 1000) for value in vector]
    vocab_size = len(vocab) + 1
    init_model()
    dataset = tf.data.Dataset.from_tensor_slices(
        (numpy.asarray(train_input_dialog_vectors), numpy.asarray(train_output_dialog_vectors))).shuffle(BUFFER_SIZE)
    dataset = dataset.apply(tf.contrib.data.batch_and_drop_remainder(BATCH_SIZE))
    train_model()
    predict_with_model()
